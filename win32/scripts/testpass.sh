##
## Copyright (c) 2013 Minoca Corp. All rights reserved.
##
## Script Name:
##
##     testpass.sh
##
## Abstract:
## 
##     This script runs the standard battery of OS tests.
##
## Author:
##
##     Evan Green 26-Oct-2013
##
## Environment:
##
##     Windows build with swiss tools
##

set -e

ROOT=${SRCROOT}/${ARCH}${VARIANT}${DEBUG}
X86ROOT=${SRCROOT}/x86${DEBUG}
ARMV7ROOT=${SRCROOT}/armv7${DEBUG}
ARMV6ROOT=${SRCROOT}/armv6${DEBUG}
$ROOT/testbin/testrtl
$ROOT/testbin/testmm
$ROOT/testbin/testc
$ROOT/testbin/testcryp

$ROOT/testbin/tdwarf $X86ROOT/bin/kernel
$ROOT/testbin/tdwarf $X86ROOT/bin/loader
$ROOT/testbin/tdwarf $X86ROOT/bin/loadefi
$ROOT/testbin/tdwarf $X86ROOT/bin/swiss
$ROOT/testbin/tdwarf $X86ROOT/tools/bin/swiss.exe
$ROOT/testbin/tdwarf $X86ROOT/tools/bin/msetup.exe
$ROOT/testbin/tdwarf $ARMV7ROOT/bin/kernel
$ROOT/testbin/tdwarf $ARMV7ROOT/bin/loadefi
$ROOT/testbin/tdwarf $ARMV7ROOT/bin/swiss
[ -f "$ARMV6ROOT/bin/kernel" ] && $ROOT/testbin/tdwarf $ARMV6ROOT/bin/kernel
[ -f "$ARMV6ROOT/bin/loadefi" ] && $ROOT/testbin/tdwarf $ARMV6ROOT/bin/loadefi
[ -f "$ARMV6ROOT/bin/swiss" ] && $ROOT/testbin/tdwarf $ARMV6ROOT/bin/swiss

$ROOT/testbin/testdisa -q $X86ROOT/bin/kernel
$ROOT/testbin/testdisa -q $X86ROOT/bin/loader
$ROOT/testbin/testdisa -q $X86ROOT/bin/loadefi
$ROOT/testbin/testdisa -q $X86ROOT/bin/swiss
$ROOT/testbin/testdisa -q $ARMV7ROOT/bin/kernel
$ROOT/testbin/testdisa -q $ARMV7ROOT/bin/loadefi
$ROOT/testbin/testdisa -q $ARMV7ROOT/bin/swiss
[ -f "$ARMV6ROOT/bin/kernel" ] && $ROOT/testbin/testdisa -q $ARMV6ROOT/bin/kernel
[ -f "$ARMV6ROOT/bin/loadefi" ] && $ROOT/testbin/testdisa -q $ARMV6ROOT/bin/loadefi
[ -f "$ARMV6ROOT/bin/loadefi" ] && $ROOT/testbin/testdisa -q $ARMV6ROOT/bin/swiss

sed 's/__attribute__((__aligned__(__alignof__([^)]*))))//g' \
    $ROOT/obj/os/lib/yy/parse.i > $ROOT/obj/os/lib/yy/parse.i2

sed 's/__attribute__((__aligned__(__alignof__([^)]*))))//g' \
    $ROOT/obj/os/lib/yy/lex.i > $ROOT/obj/os/lib/yy/lex.i2

$ROOT/testbin/yytest $ROOT/obj/os/lib/yy/parse.i2
$ROOT/testbin/yytest $ROOT/obj/os/lib/yy/lex.i2

echo "Scanning source style..."
ORIGINAL_DIRECTORY=`pwd`
cd $SRCROOT/os
ARCHES="x86 armv7 armv6"
OBJPATHS=""
for a in $ARCHES; do
    OBJPATHS="$OBJPATHS -p $SRCROOT/$a$DEBUG/obj/os "
done
python $SRCROOT/tools/resources/cstyle/style.py -c $OBJPATHS .
cd "$ORIGINAL_DIRECTORY"