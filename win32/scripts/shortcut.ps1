param ( [string]$SourceExe, [string]$Arguments, [string]$WorkingDirectory, [string]$DestinationPath )
$WshShell = New-Object -comObject WScript.Shell
$ShortcutPath = [Environment]::GetFolderPath("Desktop") + "\" + $DestinationPath + ".lnk"
$Shortcut = $WshShell.CreateShortcut($ShortcutPath)
$Shortcut.TargetPath = $SourceExe
$Shortcut.Arguments = $Arguments
$Shortcut.WorkingDirectory = $WorkingDirectory
$Shortcut.Save()