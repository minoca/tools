##
## Copyright (c) 2015 Minoca Corp. All rights reserved.
##
## Script Name:
##
##     updateswiss.sh
##
## Abstract:
## 
##     This script copies out swiss.exe to all the utilities. This script must
##     be run from swiss.exe sh, as it copies over sh.exe.
##
## Author:
##
##     Evan Green 20-Feb-2015
##
## Environment:
##
##     Windows build with swiss tools
##

cd $SRCROOT/tools/win32/
./bin/swiss.exe mkdir -p swiss
for app in `./bin/swiss.exe --list`; do
    if ./bin/swiss.exe test ./bin/swiss.exe -nt ./swiss/${app}.exe; then
        ./bin/swiss.exe cp -v ./bin/swiss.exe ./swiss/${app}.exe
    fi
done
