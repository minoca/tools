##
## Copyright (c) 2013 Minoca Corp. All rights reserved.
##
## Script Name:
##
##     setaliases.sh
##
## Abstract:
## 
##     This script sets up the build aliases.
##
## Author:
##
##     Evan Green 20-Oct-2013
##
## Environment:
##
##     Windows build with swiss tools
##

SAVE_IFS="$IFS"
IFS='
'

export SRCROOT=`echo $SRCROOT | sed 's_\\\\_/_g'`
cdshort () {
    cd $1/$2
}

run_in_background() {
  $@ &
}

for value in `cat $SRCROOT/tools/win32/scripts/aliases`; do
    alias $value
done

IFS="$SAVE_IFS"
unset SAVE_IFS
