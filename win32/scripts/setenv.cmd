@echo off
rem
rem Copyright 2013 Minoca Corp. All Rights Reserved.
rem
rem Script Name:
rem
rem     setenv.cmd
rem
rem Abstract:
rem
rem     This script sets up the Minoca build environment on Windows build
rem     machines.
rem
rem Author:
rem
rem     Evan Green 22-Oct-2013
rem
rem Environment:
rem
rem     Windows build
rem

SETLOCAL ENABLEDELAYEDEXPANSION
IF NOT DEFINED SRCROOT (
  pushd %~dp0..\..\..
  set SRCROOT=!CD!
)

set MINGWPATH=%SRCROOT%\tools\win32\mingw\bin
if "%1"=="x86" (
 set ARCH=x86
 if "%3"=="q" (
   set VARIANT=q
 )

) else (
 if "%1"=="armv7" (
  set ARCH=armv7

 ) else (
  if "%1"=="armv6" (
   set ARCH=armv6

  ) else (
   if "%1"=="x64" (
    set ARCH=x64

   ) else (
    echo Error: Usage: setenv.cmd x86^|x64^|armv7^|armv6^ dbg^|rel [variant].
    echo Examples: setenv.cmd x86 dbg
    echo           setenv.cmd armv7 rel
    pause
    exit 1
   )
  )
 )
)

if "%2"=="dbg" (
 set DEBUG=dbg

) else (
 if "%2"=="rel" (
  set DEBUG=rel

 ) else (
  echo Error: Usage: setenv.cmd x86^|x64^|armv7^|armv6^ dbg^|rel.
  echo Examples: setenv.cmd x86 dbg
  echo           setenv.cmd armv7 rel
  pause
  exit 1
 )
)

title Minoca Build %ARCH%%VARIANT% %DEBUG%
echo Minoca Build Environment for %ARCH%%VARIANT% (%DEBUG%).

set /a QEMU_MAC1=%random% %%100
set /a QEMU_MAC2=%random% %%100
set /a QEMU_MAC3=%random% %%100
set QEMU_MAC=52:54:00:%QEMU_MAC1%:%QEMU_MAC2%:%QEMU_MAC3%
set QEMU_MAC1=
set QEMU_MAC2=
set QEMU_MAC3=

IF NOT DEFINED KCOM (
  set KCOM=com1
)

set BXSHARE=%SRCROOT%\tools\win32\Bochs-2.3.7
doskey /macrofile=%SRCROOT%\tools\win32\scripts\aliases.txt
set OPENSSL_CONF=%SRCROOT%\tools\win32\bin\openssl.cfg
set PATH=%MINGWPATH%;%SRCROOT%\tools\win32\qemu-2.0.0;%SRCROOT%\tools\win32\qemu-0.13.0-windows;%SRCROOT%\%ARCH%%VARIANT%%DEBUG%\tools\bin;%SRCROOT%\%ARCH%%VARIANT%%DEBUG%\testbin;%SRCROOT%\tools\win32\scripts;%SRCROOT%\tools\win32\swiss;%SRCROOT%\tools\win32\bin;%SRCROOT%\tools\win32\ppython\App;%SRCROOT%\tools\win32\ppython\App\Scripts;%PATH%;

cd %SRCROOT%

rem Add git to the path if it's in src\git.
FOR %%i IN ("git.exe") DO SET GIT=%%~$PATH:i
if not exist "!GIT!" (
    if exist %SRCROOT%\git\cmd\git.exe (
        set GITDIR=!SRCROOT!\git
        set GITPATH=!GITDIR!\cmd;!GITDIR!\mingw32\bin;
        set PATH=!PATH!;!GITPATH!
    )

) else (
    for %%F in (!GIT!) do set GITDIR=%%~dpF\..
)

rem Start the ssh-agent if available.
FOR %%i IN ("start-ssh-agent.cmd") DO SET STARTAGENT=%%~$PATH:i
if exist "!STARTAGENT!" (
    rem Set SSH_AGENT early because start-ssh-agent.cmd erroneously uses % rather than !.
    FOR %%i IN ("ssh-agent.exe") DO @SET SSH_AGENT=%%~$PATH:i
    FOR %%i IN ("ssh-add.exe") DO @SET SSH_ADD=%%~$PATH:i
    set "OLDPATH=!PATH!"
    if not exist !SSH_ADD! (
        if exist "!GITDIR!\usr\bin\ssh-add.exe" (
            set "PATH=!PATH!;!GITDIR!\usr\bin"
            FOR %%i IN ("ssh-agent.exe") DO @SET SSH_AGENT=%%~$PATH:i
            FOR %%i IN ("ssh-add.exe") DO @SET SSH_ADD=%%~$PATH:i
        )
    )

    call "!STARTAGENT!"
    set "PATH=!OLDPATH!"
    set OLDPATH=

    rem Add any .key files in the source root if there are none so far.
    if exist "!SSH_ADD!" (
        "!SSH_ADD!" -l >nul
        if ERRORLEVEL 1 (
            FOR %%f IN (*.key) DO (
                echo Adding key %%f
                "!SSH_ADD!" %%f
            )
        )
    )
)

set GIT=
set GITDIR=
set GITPATH=
set STARTAGENT=
set SSH_AGENT=
set SSH_ADD=

rem Copy out or update swiss binaries.
%SRCROOT%\tools\win32\bin\swiss.exe sh %SRCROOT%\tools\win32\scripts\updateswiss.sh
set ENV=$SRCROOT/tools/win32/scripts/setaliases.sh
%SRCROOT%\tools\win32\swiss\sh.exe
