# Minoca OS Windows Environment
This repository contains a basic compilation environment based around MinGW/GCC. GCC, make, tar, swiss, and Qemu are included, to name a few things. In addition, this repository contains a couple of odds and ends for Minoca OS that don't really belong in either the os or third-party repositories.

The environment provided here can be used to build the tools in the [third-party](https://gitlab.com/minoca/third-party) repository, which can then in turn be used to build [Minoca OS](https://gitlab.com/minoca/os).

This environment has been tested on Windows 7 and Windows 10.

### Entering the environment
Use the [setenv.cmd](https://gitlab.com/minoca/tools/blob/master/win32/scripts/setenv.cmd) script, which takes two or three arguments: the values for ARCH, DEBUG, and optionally VARIANT. Do not unpack the environment into a path with spaces, as doing so invites the devil into our world.

The setenv.cmd script automatically detects what `SRCROOT` should be set to based on the location of the setenv.cmd script itself. It will also set `ARCH`, `DEBUG`, `VARIANT`, and `PATH` based on the arguments passed to it.

The first time setenv.cmd is run, it copies swiss.exe out to its individual utilities. The autoconf scripts in third-party require this, as they go searching out on the path for specific utilities like pwd and sed.

The setenv script also fires up an ssh-agent. A single ssh-agent process will be spun up no matter how many instances of setenv.cmd are open. If you have any files ending in .key inside of $SRCROOT, the setenv.cmd script will try to add them to the ssh-agent.

### Example
Let's say I had this repository, [third-party](https://gitlab.com/minoca/third-party), and [os](https://gitlab.com/minoca/os) checked out into `C:/src/tools`, `C:/src/third-party`, and `C:/src/os` respectively. Let's also say I wanted to build a debug version of Minoca OS for the x86 architecture. Let's create a shortcut that enters this environment.
 * Right click on the desktop, and select New -> Shortcut.
 * Enter the path for cmd.exe (usually `C:\windows\system32\cmd.exe`).
 * Enter a name for the shortcut like `Minoca x86 Debug`
 * Right click on your newly created shortcut, and select Properties
 * Modify the Target to read `C:\windows\system32\cmd.exe /k C:\src\tools\win32\scripts\setenv.cmd x86 dbg`
 * Modify Start In to read `C:\src`
 * Change the icon to something fun and retro in \windows\system32\shell32.dll. I like the tree or the RAM chip.
 * If there's a layout tab, change Screen buffer size to width of 120 and height of 9999. Change Window size to something like width 120 height 50. The default 80x25 is way too cramped.
 * If there's an Options tab, enable QuickEdit mode, and set the command history buffer size to 999.

Double click on your new shortcut, and you should be dropped in to a Bourne shell. You can now cd into the third-party directory (use the handy alias `tp` if you like) and run `make tools`. You should then be able to cd into the `os` directory (alias: `os`) and run `make` there. Type `alias` to see a list of all aliases.

### Git
You'll probably want git in your path somewhere. We like to download the portable version of Git for Windows, and unpack it to `$SRCROOT/git`. The `setenv.cmd` script will add this to the path if git is found there.

### Bugs
Bug reports and patches can be submitted to minoca-dev@googlegroups.com.

### License
GPLv3, unless otherwise noted for a particular piece of third party software.

### Contact
 * Email: minoca-dev@googlegroups.com
 * Website: [http://www.minocacorp.com/](http://www.minocacorp.com)
 * Github: [https://github.com/minoca](https://github.com/minoca)
 * Gitlab: [https://gitlab.com/minoca](https://gitlab.com/minoca)
