/*
 * Copyright (C) 2011 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <aboot/aboot.h>
#include <aboot/bootimg.h>

#define PACKED __attribute__((__packed__))
#define UBOOT_MAGIC 0x27051956
#define UBOOT_MAX_NAME 32

typedef unsigned long ULONG, *PULONG;
typedef unsigned char UCHAR, *PUCHAR;
typedef char CHAR, *PCHAR;

/*++

Structure Description:

    This structure describes the image header U-Boot is expecting at the
    beginning of the image. All data is stored *big endian* in this structure.

Members:

    Magic - Stores the magic number indicating that this is a U-Boot image.

    HeaderCrc32 - Stores the CRC32 checksum of the header structure. This field
        is assumed to be 0.

    CreationTimestamp - Stores the creation date of the image.

    DataSize - Stores the size of the image data.

    DataLoadAddress - Stores the address to load the data to.

    EntryPoint - Stores the initial address to jump to within the image.

    DataCrc32 - Stores the CRC32 checksum of only the data (not this header).

    OperatingSystem - Stores the operating system of the image.

    Architecture - Stores the CPU architecture of the image.

    ImageType - Stores the image type.

    CompressionType - Stores the compression type.

    ImageName - Stores the name of the image.

--*/

typedef struct _UBOOT_HEADER {
    ULONG Magic;
    ULONG HeaderCrc32;
    ULONG CreationTimestamp;
    ULONG DataSize;
    ULONG DataLoadAddress;
    ULONG EntryPoint;
    ULONG DataCrc32;
    UCHAR OperatingSystem;
    UCHAR Architecture;
    UCHAR ImageType;
    UCHAR CompressionType;
    CHAR ImageName[UBOOT_MAX_NAME];
} PACKED UBOOT_HEADER, *PUBOOT_HEADER;

ULONG
RtlByteSwapUlong (
    ULONG Input
    )

/*++

Routine Description:

    This routine performs a byte-swap of a 32-bit integer, effectively changing
    its endianness.

Arguments:

    Input - Supplies the integer to byte swap.

Return Value:

    Returns the byte-swapped integer.

--*/

{

    ULONG Result;

    Result = (Input & 0x000000FF) << 24;
    Result |= (Input & 0x0000FF00) << 8;
    Result |= (Input & 0x00FF0000) >> 8;
    Result |= ((Input & 0xFF000000) >> 24) & 0x000000FF;
    return Result;
}

void set_leds(unsigned led0, unsigned led1) 
{

    gpio_write(8, led0);
    if (get_omap_rev() >= OMAP_4460_ES1_DOT_0) {
        gpio_write(110, led1);
        
    } else {
        gpio_write(7, led1);
    }
    
    return;
}

int boot_image(unsigned machtype, unsigned image, unsigned len)
{
	unsigned int (*entry)(unsigned, unsigned, unsigned);
	struct boot_img_hdr *hdr = (void*) image;
	unsigned psize, pmask, kactual;
	unsigned *tag = (void*) CONFIG_ADDR_ATAGS;
	unsigned n;
	unsigned int ReturnValue;
    PUBOOT_HEADER UBootHeader;
	char *x = (void*) image;

	for (n = 0; n < 8; n++)
		if (x[n] != "ANDROID!"[n]) break;

	if (n != 8) {

        //
        // Check for the U-Boot header.
        //

        UBootHeader = (PUBOOT_HEADER)image;
        if (RtlByteSwapUlong(UBootHeader->Magic) == UBOOT_MAGIC) {
            if (RtlByteSwapUlong(UBootHeader->DataLoadAddress) !=
                CONFIG_ADDR_DOWNLOAD + sizeof(UBOOT_HEADER)) {

                printf("Warning: Load address was %x, but loaded at %x.\n",
                       RtlByteSwapUlong(UBootHeader->DataLoadAddress),
                       CONFIG_ADDR_DOWNLOAD + sizeof(UBOOT_HEADER));
            }

            entry = (void *)RtlByteSwapUlong(UBootHeader->EntryPoint);
			
			//
			// Enable the LED further from the card cage.
			//
			
			set_leds(0, 1);
            printf("Jumping to 0x%x...\n", entry);
            ReturnValue = entry(0, cfg_machine_type, CONFIG_ADDR_ATAGS);
			printf("Returned with value %x\n", ReturnValue);
        }

		printf("jumping to 0x%x...\n", CONFIG_ADDR_DOWNLOAD);
		entry = CONFIG_ADDR_DOWNLOAD;
		entry(0, cfg_machine_type, CONFIG_ADDR_ATAGS);
		for (;;);
	}

	if (len < sizeof(*hdr))
		return -1;

	psize = hdr->page_size;
	pmask = hdr->page_size - 1;

	if ((psize != 1024) && (psize != 2048) && (psize != 4096))
		return -1;

	if (len < psize)
		return -1;

	kactual = (hdr->kernel_size + pmask) & (~pmask);

	/* CORE */
	*tag++ = 2;
	*tag++ = 0x54410001;

	if (hdr->ramdisk_size) {
		*tag++ = 4;
		*tag++ = 0x54420005;
		*tag++ = image + psize + kactual;
		*tag++ = hdr->ramdisk_size;
	}

	if (hdr->cmdline && hdr->cmdline[0]) {
		/* include terminating 0 and word align */
		unsigned n = (strlen((void*) hdr->cmdline) + 4) & (~3);
		*tag++ = (n / 4) + 2;
		*tag++ = 0x54410009;
		memcpy(tag, hdr->cmdline, n);
		tag += (n / 4);
	}

	/* END */
	*tag++ = 0;
	*tag++ = 0;

	/* need to move the kernel away from the ramdisk-in-bootimg
	 * otherwise the ramdisk gets clobbered before it can be
	 * uncompressed.
	 */
	memcpy((void*) CONFIG_ADDR_KERNEL, image + psize, kactual);
	entry = (void*) CONFIG_ADDR_KERNEL;

	printf("kernel:   0x%x (%d bytes)\n",
	       CONFIG_ADDR_KERNEL, hdr->kernel_size);
	printf("ramdisk:  0x%x (%d bytes)\n",
	       image + psize + kactual, hdr->ramdisk_size);
	printf("atags:    0x%x\n", CONFIG_ADDR_ATAGS);
	printf("cmdline:  %s\n", hdr->cmdline);
	printf("machtype: %d\n", machtype);

	serial_puts("\nbooting...\n");
	entry(0, machtype, CONFIG_ADDR_ATAGS);

	serial_puts("\nreturned from kernel?\n");
	for (;;) ;

	return 0;
}
