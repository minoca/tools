
#include <aboot/aboot.h>
#include <aboot/io.h>
#include <omap4/mux.h>
#include <omap4/hw.h>

/* syslib.c */

void sr32(u32 addr, u32 start_bit, u32 num_bits, u32 value)
{
	u32 tmp, msk = 0;
	msk = 1 << num_bits;
	--msk;
	tmp = readl(addr) & ~(msk << start_bit);
	tmp |=  value << start_bit;
	writel(tmp, addr);
}

u32 wait_on_value(u32 read_bit_mask, u32 match_value, u32 read_addr, u32 bound)
{
	u32 i = 0, val;
	do {
		++i;
		val = readl(read_addr) & read_bit_mask;
		if (val == match_value)
			return (1);
		if (i == bound)
			return (0);
	} while (1);
}

void sdelay(unsigned long loops)
{
	__asm__ volatile ("1:\n" "subs %0, %1, #1\n"
			  "bne 1b":"=r" (loops):"0"(loops));
}

static void scale_tps62361(u32 reg, u32 val)
{
	u32 temp = 0;
	u32 l = 0;

	/*
	 * Select SET1 in TPS62361:
	 * VSEL1 is grounded on board. So the following selects
	 * VSEL1 = 0 and VSEL0 = 1
	 */

	/* set GPIO-7 direction as output */
	l = readl(0x4A310134);
	l &= ~(1 << TPS62361_VSEL0_GPIO);
	writel(l, 0x4A310134);

	/* set GPIO-7 data-out */
	l = 1 << TPS62361_VSEL0_GPIO;
	writel(l, 0x4A310194);

	temp = TPS62361_I2C_SLAVE_ADDR |
		(reg << PRM_VC_VAL_BYPASS_REGADDR_SHIFT) |
		(val << PRM_VC_VAL_BYPASS_DATA_SHIFT) |
		PRM_VC_VAL_BYPASS_VALID_BIT;

	writel(temp, PRM_VC_VAL_BYPASS);

	while (readl(PRM_VC_VAL_BYPASS) & PRM_VC_VAL_BYPASS_VALID_BIT)
                ;
}

void scale_vcores(void)
{

    unsigned int rev = get_omap_rev();
    u32 volt;
    
    enable_all_clocks();
    
	/* For VC bypass only VCOREx_CGF_FORCE  is necessary and
	 * VCOREx_CFG_VOLTAGE  changes can be discarded
	 */
	/* PRM_VC_CFG_I2C_MODE */
	writel(0x0, 0x4A307BA8);
	/* PRM_VC_CFG_I2C_CLK */
	writel(0x6026, 0x4A307BAC);

    if (rev >= OMAP_4460_ES1_DOT_0) {
        volt = (1300 - TPS62361_BASE_VOLT_MV) / 10;
        scale_tps62361(TPS62361_REG_ADDR_SET1, volt);
    }
    
	/* set VCORE1 force VSEL */
	/* PRM_VC_VAL_BYPASS) */
    if (rev >= OMAP_4460_ES1_DOT_0) {
        writel(0x305512, 0x4A307BA0);
        
    } else {
        writel(0x3A5512, 0x4A307BA0);
    }

	writel(readl(0x4A307BA0) | 0x1000000, 0x4A307BA0);
	while(readl(0x4A307BA0) & 0x1000000)
		;

	/* PRM_IRQSTATUS_MPU */
	writel(readl(0x4A306010), 0x4A306010);


	/* FIXME: set VCORE2 force VSEL, Check the reset value */
	/* PRM_VC_VAL_BYPASS) */
    if (rev >= OMAP_4460_ES1_DOT_0) {
        writel(0x305B12, 0x4A307BA0);
        
    } else {
        writel(0x295B12, 0x4A307BA0);
    }
	
	writel(readl(0x4A307BA0) | 0x1000000, 0x4A307BA0);
	while(readl(0x4A307BA0) & 0x1000000)
		;

	/* PRM_IRQSTATUS_MPU */
	writel(readl(0x4A306010), 0x4A306010);

	/*set VCORE3 force VSEL */
	/* PRM_VC_VAL_BYPASS */
    if (rev >= OMAP_4460_ES1_DOT_0) {
        return;
    }
    
	writel(0x2A6112, 0x4A307BA0);

	writel(readl(0x4A307BA0) | 0x1000000, 0x4A307BA0);

	while(readl(0x4A307BA0) & 0x1000000)
		;

	/* PRM_IRQSTATUS_MPU */
	writel(readl(0x4A306010), 0x4A306010);
}
