##
## Copyright 2012 Evan Green. All Rights Reserved.
##
## Module Name:
##
##     style.py
##
## Abstract:
##
##     This module implements the C style checker.
##
## Author:
##
##     Evan Green 20-Nov-2012
##
## Environment:
##
##     Python 2.7
##

##
## -------------------------------------------------------------------- Imports
##

import argparse
import os
from pycparser import c_parser, c_ast
import re
import string
import subprocess
import sys
import time

##
## ---------------------------------------------------------------- Definitions
##

##
## Define numbers for each of the violations.
##

VIOLATION_LINE_ENDING = 1
VIOLATION_TAB = 2
VIOLATION_TRAILING_SPACE = 3
VIOLATION_EXTRA_BLANK_LINES = 4
VIOLATION_FUNCTION_DESCRIPTION_SENTENCE = 5
VIOLATION_FUNCTION_THIS_ROUTINE = 6
VIOLATION_FUNCTION_ARGUMENT_SENTENCE = 7
VIOLATION_FUNCTION_RETURN_SENTENCE = 8
VIOLATION_MODULE_DESCRIPTION = 9
VIOLATION_FILE_NAME_MISMATCH = 10
VIOLATION_LINE_TOO_LONG = 11
VIOLATION_KEYWORD_WITHOUT_SPACE = 12
VIOLATION_KEYWORD_WITHOUT_SEMICOLON = 13
VIOLATION_KEYWORD_NOT_FIRST_ON_LINE = 14
VIOLATION_FUNCTION_COMMENT_MISSING = 15
VIOLATION_ARGUMENT_COMMENT_MISSING = 16
VIOLATION_LOCAL_NOT_AT_FUNCTION_BEGIN = 17
VIOLATION_LOCAL_NAME_TOO_SHORT = 18
VIOLATION_LOCAL_NAME_NOT_CAPTITALIZED = 19
VIOLATION_ARGUMENT_NAME_TOO_SHORT = 20
VIOLATION_ARGUMENT_NAME_NOT_CAPTITALIZED = 21
VIOLATION_LOCALS_NOT_ALPHABETIZED = 22
VIOLATION_PLUS_PLUS = 23
VIOLATION_MINUS_MINUS = 24
VIOLATION_BLANK_LINE_NEEDED_AFTER_MULTILINE = 25
VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_ASSERT = 26
VIOLATION_BLANK_LINE_NEEDED_AFTER_CLOSING_CURLY = 27
VIOLATION_FUNCTION_HAS_NO_STATEMENTS = 28
VIOLATION_BLANK_LINE_NEEDED_BEFORE_FIRST_LINE = 29
VIOLATION_BLANK_LINE_NEEDED_BEFORE_ELSE = 30
VIOLATION_UNNECESSARY_BLANK_LINE = 31
VIOLATION_ELSE_ON_ITS_OWN_LINE = 32
VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_COMMENT = 33
VIOLATION_OPENING_FUNCTION_BLANK_NEEDED = 34
VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_PREPROCESSOR = 35
VIOLATION_BLANK_LINE_NEEDED_BEFORE_LABEL = 36
VIOLATION_BLANK_LINE_NEEDED_BEFORE_CASE = 37
VIOLATION_BLANK_LINE_NEEDED_BEFORE_DEFAULT = 38
VIOLATION_LONE_OPENING_CURLY = 39
VIOLATION_BLANK_LINE_NEEDED_BEFORE_WHILE_AFTER_DO = 40
VIOLATION_ILLEGAL_WHITESPACE = 41

VIOLATION_COUNT = 42

##
## Define descriptions for each of the error codes.
##

ViolationDescriptions = [
    "Internal error: Not a valid violation!",
    "Found a Unix line ending (\n). Please use Windows line endings (\r\n).",
    "Found a tab.",
    "Found trailing whitespace.",
    "Too many consecutive blank lines.",
    "Function descriptions must be full sentences that end in a period.",
    "Function descriptions must start with \"This routine\".",
    "Function argument descriptions must be full sentences that end in a "
        "period.",
    "Function return value descriptions must be full sentences that end in a "
        "period.",
    "File is missing a module header comment.",
    "Module name in file header comment does not match actual file name.",
    "Line exceeds 80 characters.",
    "Keyword if/for/do/while/switch/case needs space after it.",
    "Keyword break/continue must be immediately followed by a semicolon.",
    "Keyword must be at the beginning of a line.",
    "Function is missing \"Routine Description\" comment.",
    "Function argument comment missing.",
    "Local variables should all be declared at the top of the function.",
    "Local variable name too short.",
    "Local variable name not CamelCased.",
    "Function argument name too short.",
    "Function argument name not CamelCased.",
    "Local variables must be alphabetized.",
    "Avoid using the ++ operator.",
    "Avoid using the -- operator.",
    "Blank line missing after multi-line statement.",
    "Blank lines are required before and after any block of asserts.",
    "A blank line is required after a block of lone curly braces.",
    "Functions are required to have a statement (hint: add a return).",
    "A blank line is needed before the first statement.",
    "A blank line is needed before an else statement.",
    "Remove unnecessary blank line.",
    "Else statement should not be on its own line, should be \"} else {\".",
    "Blank lines are needed surrounding a comment.",
    "Blank line needed after opening brace of function definition.",
    "Blank lines needed surrounding any block of preprocessor directives.",
    "Blank line needed before label.",
    "Blank line needed before each cluster of case statements.",
    "Blank line needed before default.",
    "Opening curly brace should be on the same line as the conditional.",
    "A blank line is needed before a while that ends a do.",
    "Illegal whitespace character.",
]

assert(len(ViolationDescriptions) == VIOLATION_COUNT)

##
## -------------------------------------------------------------------- Globals
##

##
## Pre-compile the regular expressions used.
##

AlignOfExpression = re.compile(r'__alignof__ *\(.*?\)')
AlignExpression = re.compile(r'__aligned__ *\(.*?\)')
AttributesExpression = re.compile(r'__attribute__ *\(\(.*?\)\)')
OffsetOfExpression = re.compile(r'offsetof *\(.*?\)')
AlignedExpression = \
    re.compile(r'__attribute__ *\(\( *(aligned|visibility) *\(.*?\)\)\)')

VaArgExpression = re.compile(r'__builtin_va_arg.*?;', re.DOTALL)
BuiltinFuncExpression = re.compile(r'__builtin_\w*', re.DOTALL)
AsmExpression = re.compile(r' (asm|__asm__)\s*'
                           r'((volatile|__volatile__)\s*)?\(.*?\);',
                           re.DOTALL)

UnixLineEndingsExpression = re.compile(r'[^\r]\n')
TabsExpression = re.compile(r'\t')
IllegalWhitespaceExpression = re.compile(r'\r\f\v')
TrailingWhitespaceExpression = re.compile(r'[ \t\r\f\v]\n')
ExtraBlankLinesExpression = re.compile(r'\n{3,}')
LineLimitExpression = re.compile(r'[^\n]{81,}')
BlankLineExpression = re.compile(r'\n(?=\n)')

ClosingCurlyExpression = re.compile(r'\n[ \t]*?\}((?=\n)|( *while.*?;))',
                                    re.DOTALL)

PreprocessorExpression = re.compile(r'\n[#][ \t]*?[a-z]')
AssertExpression = \
    re.compile(r'\n *[0-9A-Za-z_]*((ASSERT)|(assert)).*?;', re.DOTALL)

CorrectCommentExpression = re.compile(r'//\n( *//.*?\n)+ *//(?=\n)')
SingleLineCommentExpression = re.compile(r'//')
MultiLineCommentExpression = re.compile(r'/\*.*?\*/', re.DOTALL)
ElseExpression = re.compile(r'[ \}]else[ \{]')
WhileAfterDoExpression = re.compile(r'\} *while *\(')
CaseExpression = re.compile(r' case ')
SwitchExpression = re.compile(r' switch[ \(]')
DefaultExpression = re.compile(r' default[ :]')
KeywordSpaceExpression = \
    re.compile(r'(( do\{)|( for\()|( if\()|( while\()|( switch\())[^ ]')

KeywordSemicolonExpression = re.compile(r'(( break[ \n])|( continue[ \n]))')
SpaceKeywordExpression = re.compile(
    r'\n +?[^ \n]+?.*?(( do[ \{])|( for[ \(])|( switch[ \(])|( case )|'
    r'( break[ ;])|( continue[ ;])|( return )|( goto )|( default ))')

BeginElseExpression = re.compile(r'\n[ ]*else')
ModuleDescriptionExpression = re.compile(
    r'^/\*\+\+\n\nCopyright \(c\) (20\d\d) .+?\n\n'
    r'Module Name:\n\n    (.+?)\n\n'
    r'Abstract:\n\n    (.+?)\n\n'
    r'Author:\n\n    ([^\d]+) (\((.+?)\) )?(.+?)\n.+'
    r'Environment:\n\n    (.+?)\n\n\-\-\*/\n\n',
    re.DOTALL)

HeaderDescriptionExpression = re.compile(
    r'^/\*\+\+\n\nCopyright \(c\) (20\d\d) .+?\n\n'
    r'Module Name:\n\n    (.+?)\n\n'
    r'Abstract:\n\n    (.+?)\n\n'
    r'Author:\n\n    ([^\d]+) (\((.+?)\) )?(.+?)\n\n\-\-\*/\n\n',
    re.DOTALL)

FunctionCommentExpression = re.compile(
    r'\n\n/\*\+\+\n\nRoutine Description:\n\n    (.+?)\n\n'
    r'Arguments:\n\n(.+?)'
    r'\n\nReturn Value:\n\n    (.+?)\n\n\-\-\*/\n\n',
    re.DOTALL)

FunctionArgumentExpression = re.compile(
    r'    ([\w\.]*) - (Supplies .*?)'
    r'(?=\n\n    [\w\.]|$)',
    re.DOTALL)

LoneElseExpression = re.compile(r'\} *else *\{')
DoExpression = re.compile(r'do *\{$')
ConditionalExpression =\
re.compile(r'((if)|(for)|(while)|(switch)|(\} else if)) ?\(')

MultiLineCommentBegin = re.compile(r'/\*')
MultiLineCommentEnd = re.compile(r'\*/')
SingleLineBegin = re.compile(r'//')
DoubleString = re.compile(r'".+?"')
SingleString = re.compile(r"'.+?'")

##
## ------------------------------------------------------------------ Functions
##

class StyleViolation:
    def __init__ (
        self,
        CodeNumber,
        FileName,
        LineNumber,
        Parameter=None
        ):

        self.code = CodeNumber
        self.file = FileName
        self.line = LineNumber
        self.parameter = Parameter
        return

    def __repr__(
        self
        ):

        return str(self.__unicode__())

    def __str__ (
        self
        ):

        return str(self.__unicode__())

    def __unicode__ (
        self
        ):

        Description =  u"%s:%d: %s" % (self.file,
                                       self.line,
                                       ViolationDescriptions[self.code])

        if (self.parameter is not None):
            Description += ' (' + str(self.parameter) + ')'

        return Description

def main (
    ):

    StartTime = time.time()
    Results = {'violations': [],
               'files_processed': 0,
               'files_skipped': 0,
               'files_ignored': 0}

    ##
    ## Parse the arguments.
    ##

    ArgParser = argparse.ArgumentParser(description="Check source code style.")
    ArgParser.add_argument('-p',
                           '--path',
                           help='Path to search for preprocessed source '
                                '(.i files)',
                           action='append',
                           default=[])

    ArgParser.add_argument('-q',
                           '--quiet',
                           help='Suppress non-error output.',
                           action='store_true')

    ArgParser.add_argument('-c',
                           '--changed',
                           help='Scan only files reported by SVN as changed.',
                           action='store_true')

    ArgParser.add_argument('-s',
                           '--skip',
                           help='Skip the given file or directory',
                           action='append',
                           default=[])

    ArgParser.add_argument('files',
                           help='Files to directories to scan',
                           nargs='*')

    Arguments = ArgParser.parse_args()

    ##
    ## Get the output from git status if the caller only wants changed files.
    ##

    if (Arguments.changed != False):
        Items = []
        GitArguments = ['git', 'status', '--porcelain']
        GitArguments += Arguments.files
        SvnProcess = subprocess.Popen(GitArguments,
                                      stdout=subprocess.PIPE,
                                      shell=True)

        GitStatus = SvnProcess.communicate()[0]
        SvnProcess.wait()
        GitStatus = string.replace(GitStatus, '\r\n', '\n')
        GitLines = GitStatus.split('\n')
        for GitLine in GitLines:
            if (len(GitLine) > 3):
                Status = GitLine[0]
                if (Status == ' '):
                    Status = GitLine[1]

                if (((Status == 'M') or (Status == 'A') or
                     (Status == 'R') or (Status == 'C'))):

                    GitLine = GitLine[3:]
                    Arrow = string.rfind(GitLine, ' -> ')
                    if (Arrow != -1):
                        GitLine = GitLine[Arrow + 4:]

                    if GitLine[0] == '"':
                        GitLine = GitLine[1:-1].decode('string-escape')

                    Items.append(GitLine)

    ##
    ## Evaluate all files and directories the caller specified.
    ##

    else:
        Items = Arguments.files

    ##
    ## Loop through and scan each file or directory.
    ##

    for Item in Items:
        if Item in Arguments.skip:
            continue

        if (os.path.isfile(Item)):
            ScanSingleFile(Item, Arguments, Results)

        elif (os.path.isdir(Item)):

            ##
            ## Loop recursively through every file and folder in the given
            ## directory.
            ##

            for Root, Directories, Files in os.walk(Item):

                ##
                ## Remove git or svn directories.
                ##

                if '.git' in Directories:
                    Directories.remove('.git')

                if '.svn' in Directories:
                    Directories.remove('.svn')

                ##
                ## Loop through all files.
                ##

                for File in Files:
                    CombinedPath = os.path.join(Root, File)
                    if (CombinedPath in Arguments.skip):
                        continue

                    ScanSingleFile(CombinedPath, Arguments, Results)

        elif (not os.path.exists(Item)):
            print("Error: %s does not exist." % Item)
            Results['files_skipped'] += 1

        else:
            print("Error: %s is unknown type" % Item)
            Results['files_skipped'] += 1

    ##
    ## Print out the results.
    ##

    Violations = Results['violations']
    if (len(Violations) != 0):
        if (len(Violations) == 1):
            print("\nFound 1 violation: ")

        else:
            print("\nFound %d violations:" % len(Violations))

        for Violation in Violations:
            print(str(Violation))

    elif (Arguments.quiet == False):
        print("\nNo violations found.")

    Duration = time.time() - StartTime
    if (Arguments.quiet == False):
        print("\nScanned %d files, Skipped %d files, Ignored %d files" %
              (Results['files_processed'],
               Results['files_skipped'],
               Results['files_ignored']))

        print("Duration: %.2f seconds" % Duration)

    if (len(Violations) != 0):
        return 2

    if (Results['files_skipped'] != 0):
        return 1

    return 0

##
## --------------------------------------------------------- Internal Functions
##

def ScanSingleFile (
    Path,
    Arguments,
    Results
    ):

    PreprocessedPath = None
    if (Path[-2:] == '.c'):
        PreprocessedPath = FindPreprocessedSource(Path, Arguments)
        if (PreprocessedPath is None):
            print("Cannot find preprocessed source for %s." % Path)
            Results['files_skipped'] += 1
            return

    elif (Path[-2:] != '.h'):
        if (Arguments.quiet == False):
            print("Ignoring %s" % Path)
            Results['files_ignored'] += 1

        return

    ScanFile(Path, PreprocessedPath, Arguments, Results)
    return

def FindPreprocessedSource (
    Path,
    Arguments
    ):

    PreprocessedFile = Path[:-2] + '.i'
    FileName = os.path.basename(PreprocessedFile)
    Paths = list(Arguments.path)
    ParentName = os.path.basename(os.path.dirname(PreprocessedFile))
    OneUpDirectory = os.path.dirname(os.path.dirname(PreprocessedFile))

    ##
    ## Get the name of the directory this source is in. If it's an architecture,
    ## look to see if that's in any of the paths, and reorder the list to try
    ## that path first.
    ##

    for Directory in Paths:
        if (ParentName in Directory):
            Paths.remove(Directory)
            Paths.insert(0, Directory)

    ##
    ## Try the path directly first.
    ##

    for Directory in Paths:
        Potential = os.path.join(Directory, PreprocessedFile)
        if (os.path.isfile(Potential)):
            return Potential

    ##
    ## Try anything within the directory.
    ##

    for Directory in Paths:
        Potential = os.path.join(Directory, PreprocessedFile)
        Result = FindFileInPath(os.path.dirname(Potential), FileName)
        if (Result is not None):
            return Result

    ##
    ## Try one up from the directory.
    ##

    for Directory in Paths:
        Directory = os.path.join(Directory, OneUpDirectory)
        Result = FindFileInPath(Directory, FileName)
        if (Result is not None):
            return Result

    return None

def FindFileInPath (
    Path,
    FileName
    ):

    for Root, Directories, Files in os.walk(Path):
        if '.git' in Directories:
            Directories.remove('.git')

        if '.svn' in Directories:
            Directories.remove('.svn')

        for File in Files:
            if (File == FileName):
                return os.path.join(Root, File)

    return None

def ScanFile (
    UnprocessedFilePath,
    PreprocessedFilePath,
    Arguments,
    Results
    ):

    ##
    ## Load and analyze the file.
    ##

    UnprocessedFile, PreprocessedFile = LoadFile(UnprocessedFilePath,
                                                 PreprocessedFilePath)

    if (UnprocessedFile is None):
        Results['files_skipped'] += 1
        return

    Result = AnalyzeFileBuffers(UnprocessedFilePath,
                                UnprocessedFile,
                                PreprocessedFile)

    ##
    ## Fix up the violations to contain the full file name from the path root.
    ## If the violation is not in the file being scanned (it is in an include),
    ## then skip it.
    ##

    FileName = os.path.basename(UnprocessedFilePath)
    Violations = Result['Violations']
    FileViolations = []
    for Violation in Violations:
        if (Violation.file.endswith(FileName)):
            Violation.file = UnprocessedFilePath
            FileViolations.append(Violation)

    if (len(FileViolations) != 0):
        print("File: %s - %d violations" %
              (UnprocessedFilePath, len(FileViolations)))

    elif (Arguments.quiet == False):
        print("File: %s - OK" % UnprocessedFilePath)

    Results['violations'] += FileViolations
    Results['files_processed'] += 1
    return

def LoadFile (
    UnprocessedFilePath,
    PreprocessedFilePath
    ):

    ##
    ## Open up both the original C file and the preprocessed version.
    ##
    try:
        UnprocessedFile = open(UnprocessedFilePath, 'rb')
        UnprocessedFileContents = UnprocessedFile.read()
        UnprocessedFile.close()

    except IOError:
        print("Failed to open %s" % UnprocessedFilePath)
        return (None, None)

    PreprocessedFileContents = None
    if (PreprocessedFilePath is not None):
        try:
            PreprocessedFile = open(PreprocessedFilePath, 'rU')
            PreprocessedFileContents = PreprocessedFile.read()
            PreprocessedFile.close()
        except IOError:
            print("Failed to open %s" % UnprocessedFilePath)
            return (None, None)

        ##
        ## Remove the GCC extensions from the preprocessed file.
        ##

        PreprocessedFileContents = \
           string.replace(PreprocessedFileContents, '__builtin_va_list', 'int')

        PreprocessedFileContents = \
                     string.replace(PreprocessedFileContents, '__inline__', '')

        PreprocessedFileContents = \
                     string.replace(PreprocessedFileContents, '__inline', '')

        PreprocessedFileContents = \
                   string.replace(PreprocessedFileContents, '__restrict__', '')

        PreprocessedFileContents = \
                  string.replace(PreprocessedFileContents, '__extension__', '')

        PreprocessedFileContents = \
               string.replace(PreprocessedFileContents, '__builtin_inf()', '0')

        PreprocessedFileContents = \
             string.replace(PreprocessedFileContents, '__builtin_nan("")', '0')

        PreprocessedFileContents = re.sub(AlignOfExpression,
                                          '',
                                          PreprocessedFileContents)

        PreprocessedFileContents = re.sub(AlignExpression,
                                          '',
                                          PreprocessedFileContents)

        PreprocessedFileContents = re.sub(AlignedExpression,
                                          '',
                                          PreprocessedFileContents)

        PreprocessedFileContents = re.sub(AttributesExpression,
                                          '',
                                          PreprocessedFileContents)

        PreprocessedFileContents = re.sub(OffsetOfExpression,
                                          '0',
                                          PreprocessedFileContents)

        PreprocessedFileContents = re.sub(VaArgExpression,
                                          '0;',
                                          PreprocessedFileContents)

        PreprocessedFileContents = re.sub(BuiltinFuncExpression,
                                          '((void *)0)',
                                          PreprocessedFileContents)

        PreprocessedFileContents = re.sub(AsmExpression,
                                          '0 = 0;',
                                          PreprocessedFileContents)

        PreprocessedFileContents = \
                   string.replace(PreprocessedFileContents, '__volatile__', '')

        PreprocessedFileContents = \
                       string.replace(PreprocessedFileContents, '__thread', '')

    return (UnprocessedFileContents, PreprocessedFileContents)

def AnalyzeFileBuffers (
    FilePath,
    UnprocessedFile,
    PreprocessedFile
    ):

    Result = {}
    Violations = []

    ##
    ## First, look for non-Windows line endings.
    ##

    UnixLineEndings = UnixLineEndingsExpression.finditer(UnprocessedFile)
    for UnixLineEndingMatch in UnixLineEndings:
        LineNumber = GetLineFromMatch(UnprocessedFile, UnixLineEndingMatch)
        Violation = StyleViolation(VIOLATION_LINE_ENDING, FilePath, LineNumber)
        Violations.append(Violation)

    ##
    ## Now convert all line endings to Unix.
    ##

    UnprocessedFile = string.replace(UnprocessedFile, '\r\n', '\n')

    ##
    ## Look for tabs.
    ##

    for TabMatch in TabsExpression.finditer(UnprocessedFile):
        LineNumber = GetLineFromMatch(UnprocessedFile, TabMatch)
        Violation = StyleViolation(VIOLATION_TAB, FilePath, LineNumber)
        Violations.append(Violation)

    for TabMatch in IllegalWhitespaceExpression.finditer(UnprocessedFile):
        LineNumber = GetLineFromMatch(UnprocessedFile, TabMatch)
        Violation = StyleViolation(VIOLATION_ILLEGAL_WHITESPACE,
                                   FilePath,
                                   LineNumber)

        Violations.append(Violation)

    ##
    ## Look for trailing whitespace.
    ##

    TrailingWhitespaceMatches = \
                         TrailingWhitespaceExpression.finditer(UnprocessedFile)

    for TrailingWhitespaceMatch in TrailingWhitespaceMatches:
        LineNumber = GetLineFromMatch(UnprocessedFile, TrailingWhitespaceMatch)
        Violation = StyleViolation(VIOLATION_TRAILING_SPACE,
                                   FilePath,
                                   LineNumber)

        Violations.append(Violation)

    ##
    ## Look for more than one blank line.
    ##

    for ExtraBlank in ExtraBlankLinesExpression.finditer(UnprocessedFile):
        LineNumber = GetLineFromMatch(UnprocessedFile, ExtraBlank)
        Violation = StyleViolation(VIOLATION_EXTRA_BLANK_LINES,
                                   FilePath,
                                   LineNumber)

        Violations.append(Violation)

    ##
    ## Look for lines over 80 characters.
    ##

    for LineLimitMatch in LineLimitExpression.finditer(UnprocessedFile):
        LineNumber = GetLineFromMatch(UnprocessedFile, LineLimitMatch)
        Violation = StyleViolation(VIOLATION_LINE_TOO_LONG,
                                   FilePath,
                                   LineNumber)

        Violations.append(Violation)

    ##
    ## Create a version of the unprocessed file that doesn't have any stuff in
    ## the comments.
    ##

    CommentlessFile = StripComments(UnprocessedFile)

    ##
    ## Create the list of blank lines.
    ##

    BlankLineList = []
    for BlankLineMatch in BlankLineExpression.finditer(UnprocessedFile):
        BlankLineList.append(
                          GetLineFromMatch(UnprocessedFile, BlankLineMatch) + 1)

    ##
    ## Create a list of lone closing curly braces from the commentless file.
    ## Do while loops count as curlies that need a blank line after them.
    ##

    ClosingCurlyList = []
    for ClosingCurlyMatch in ClosingCurlyExpression.finditer(CommentlessFile):
        LineNumber = GetLineFromMatch(CommentlessFile,
                                      ClosingCurlyMatch,
                                       UseMatchEnd=True)

        ClosingCurlyList.append(LineNumber)

    ##
    ## Create a list of preprocessor statements.
    ##

    PreprocessorList = []
    for PreprocessorMatch in PreprocessorExpression.finditer(CommentlessFile):
        PreprocessorList.append(
                      GetLineFromMatch(CommentlessFile, PreprocessorMatch) + 1)

    ##
    ## Create a list of asserts.
    ##

    AssertList = []
    for AssertMatch in AssertExpression.finditer(CommentlessFile):
        FirstLine = GetLineFromMatch(CommentlessFile, AssertMatch) + 1
        LastLine = GetLineFromMatch(CommentlessFile,
                                    AssertMatch,
                                    UseMatchEnd=True)

        AssertList.append((FirstLine, LastLine))

    ##
    ## Create a list of correctly formed comments.
    ##

    CorrectCommentList = []
    for CommentMatch in CorrectCommentExpression.finditer(UnprocessedFile):
        FirstLine = GetLineFromMatch(UnprocessedFile, CommentMatch)
        LastLine = GetLineFromMatch(UnprocessedFile,
                                    CommentMatch,
                                    UseMatchEnd=True)

        CorrectCommentList.append((FirstLine, LastLine))

    ##
    ## Create a list of all single line comments.
    ##

    SingleLineCommentList = []
    for CommentMatch in SingleLineCommentExpression.finditer(UnprocessedFile):
        SingleLineCommentList.append(
                               GetLineFromMatch(UnprocessedFile, CommentMatch))

    SingleLineCommentList = list(set(SingleLineCommentList))
    SingleLineCommentList.sort()

    ##
    ## Create a list of all multiline comments.
    ##

    MultiLineCommentList = []
    for CommentMatch in MultiLineCommentExpression.finditer(CommentlessFile):
        FirstLine = GetLineFromMatch(CommentlessFile, CommentMatch)
        LastLine = GetLineFromMatch(CommentlessFile,
                                    CommentMatch,
                                    UseMatchEnd=True)

        MultiLineCommentList.append((FirstLine, LastLine))

    ##
    ## Create a list of all elses.
    ##

    ElseList = []
    for ElseMatch in ElseExpression.finditer(CommentlessFile):
        ElseList.append(GetLineFromMatch(CommentlessFile, ElseMatch))

    ##
    ## Create a list of all whiles that end a do-while.
    ##

    WhileAfterDoList = []
    for WhileAfterDoMatch in WhileAfterDoExpression.finditer(CommentlessFile):
        LineNumber = GetLineFromMatch(CommentlessFile, WhileAfterDoMatch)
        WhileAfterDoList.append(LineNumber)

    ##
    ## Create a list of all cases.
    ##

    CaseList = []
    for CaseMatch in CaseExpression.finditer(CommentlessFile):
        CaseList.append(GetLineFromMatch(CommentlessFile, CaseMatch))

    ##
    ## Create a list of all switches.
    ##

    SwitchList = []
    for SwitchMatch in SwitchExpression.finditer(CommentlessFile):
        SwitchList.append(GetLineFromMatch(CommentlessFile, SwitchMatch))

    ##
    ## Create a list of all defaults (like a default case).
    ##

    DefaultList = []
    for DefaultMatch in DefaultExpression.finditer(CommentlessFile):
        DefaultList.append(GetLineFromMatch(CommentlessFile, DefaultMatch))

    ##
    ## Look for keywords that need spaces after them that might not have it.
    ##

    for KeywordSpaceMatch in KeywordSpaceExpression.finditer(CommentlessFile):
        LineNumber = GetLineFromMatch(CommentlessFile, KeywordSpaceMatch)
        Violation = StyleViolation(VIOLATION_KEYWORD_WITHOUT_SPACE,
                                   FilePath,
                                   LineNumber)

        Violations.append(Violation)

    ##
    ## Look for keywords that must not have spaces after them.
    ##

    KeywordSemicolonMatches = \
                           KeywordSemicolonExpression.finditer(CommentlessFile)

    for KeywordSemicolonMatch in KeywordSemicolonMatches:
        LineNumber = GetLineFromMatch(CommentlessFile, KeywordSemicolonMatch)
        Violation = StyleViolation(VIOLATION_KEYWORD_WITHOUT_SEMICOLON,
                                   FilePath,
                                   LineNumber)

        Violations.append(Violation)

    ##
    ## Look for keywords that must begin on their own line.
    ##

    for SpaceKeywordMatch in SpaceKeywordExpression.finditer(CommentlessFile):
        LineNumber = GetLineFromMatch(CommentlessFile, SpaceKeywordMatch) + 1
        Violation = StyleViolation(VIOLATION_KEYWORD_NOT_FIRST_ON_LINE,
                                   FilePath,
                                   LineNumber,
                                   SpaceKeywordMatch.group(1))

        Violations.append(Violation)

    ##
    ## Look for any keywords that must not begin on their own line.
    ##

    for BeginElseMatch in BeginElseExpression.finditer(CommentlessFile):
        LineNumber = GetLineFromMatch(CommentlessFile, BeginElseMatch) + 1
        Violation = StyleViolation(VIOLATION_ELSE_ON_ITS_OWN_LINE,
                                   FilePath,
                                   LineNumber)

        Violations.append(Violation)

    ##
    ## Get the module description information.
    ##

    ModuleDescription = {}
    if (FilePath[-2:] == '.c'):
        ModuleDescriptionMatch = \
                            ModuleDescriptionExpression.match(UnprocessedFile)

    elif (FilePath[-2:] == '.h'):
        ModuleDescriptionMatch = \
                            HeaderDescriptionExpression.match(UnprocessedFile)

    else:

        assert False

    if (ModuleDescriptionMatch is None):
        Violation = StyleViolation(VIOLATION_MODULE_DESCRIPTION, FilePath, 1)
        Violations.append(Violation)

    else:
        ModuleCopyrightYear = ModuleDescriptionMatch.group(1)
        ModuleName = ModuleDescriptionMatch.group(2)
        ModuleAbstract = ModuleDescriptionMatch.group(3)
        ModuleAuthor = ModuleDescriptionMatch.group(4)
        ModuleAuthorEmail = ModuleDescriptionMatch.group(6)
        ModuleCreationDate = ModuleDescriptionMatch.group(7)
        ModuleEnvironment = ''
        if (FilePath[-2:] == '.c'):
            ModuleEnvironment = ModuleDescriptionMatch.group(8)

        ##
        ## Check the listed module name against the file name, ignoring case.
        ##

        if (ModuleName.lower() != os.path.basename(FilePath).lower()):
            Violation = StyleViolation(VIOLATION_FILE_NAME_MISMATCH,
                                       FilePath,
                                       1)

            Violations.append(Violation)

        ModuleDescription['CopyrightYear'] = int(ModuleCopyrightYear)
        ModuleDescription['Name'] = ModuleName
        ModuleDescription['Abstract'] = \
                                  string.replace(ModuleAbstract, '\n    ', ' ')

        ModuleDescription['Author'] = ModuleAuthor
        ModuleDescription['AuthorEmail'] = ModuleAuthorEmail
        ModuleDescription['CreationDate'] = ModuleCreationDate
        ModuleDescription['Environment'] = ModuleEnvironment

    ##
    ## Find all the "Routine Description" comments.
    ##

    FunctionCommentMatches = FunctionCommentExpression.finditer(UnprocessedFile)
    FunctionComments = []
    for FunctionCommentMatch in FunctionCommentMatches:
        LineNumber = UnprocessedFile.count("\n",
                                           0,
                                           FunctionCommentMatch.start()) + 1

        FunctionComment = {'LineNumber': LineNumber}

        ##
        ## Get all the arguments for this function.
        ##

        ArgumentsString = FunctionCommentMatch.group(2)
        ArgumentMatches = FunctionArgumentExpression.finditer(ArgumentsString)
        Arguments = []
        for ArgumentMatch in ArgumentMatches:
            ArgumentDescription = string.replace(ArgumentMatch.group(2),
                                                 '\n        ',
                                                 ' ')

            if (ArgumentDescription[-1:] != '.'):
                LineNumber = GetLineFromMatch(UnprocessedFile,
                                              FunctionCommentMatch)

                Violation = \
                        StyleViolation(VIOLATION_FUNCTION_ARGUMENT_SENTENCE,
                                       FilePath,
                                       LineNumber)

                Violations.append(Violation)

            Arguments.append((ArgumentMatch.group(1), ArgumentDescription))

        ##
        ## Get the function description.
        ##

        FunctionDescription = string.replace(FunctionCommentMatch.group(1),
                                             '\n    ',
                                             ' ')

        if (re.match(r'This routine[ ,]', FunctionDescription) is None):
            LineNumber = GetLineFromMatch(UnprocessedFile, FunctionCommentMatch)
            Violation = StyleViolation(VIOLATION_FUNCTION_THIS_ROUTINE,
                                       FilePath,
                                       LineNumber)

            Violations.append(Violation)

        if (FunctionDescription[-1:] != '.'):
            LineNumber = GetLineFromMatch(UnprocessedFile, FunctionCommentMatch)
            Violation = StyleViolation(VIOLATION_FUNCTION_DESCRIPTION_SENTENCE,
                                       FilePath,
                                       LineNumber)

            Violations.append(Violation)

        ##
        ## Get the return value.
        ##

        FunctionReturn = string.replace(FunctionCommentMatch.group(3),
                                        '\n    ',
                                        ' ')

        if (FunctionReturn[-1:] != '.'):
            LineNumber = GetLineFromMatch(UnprocessedFile, FunctionCommentMatch)
            Violation = StyleViolation(VIOLATION_FUNCTION_RETURN_SENTENCE,
                                       FilePath,
                                       LineNumber)

            Violations.append(Violation)

        FunctionComment['Description'] = FunctionDescription
        FunctionComment['Arguments'] = Arguments
        FunctionComment['ReturnValue'] = FunctionReturn
        FunctionComments.append(FunctionComment)

    if (PreprocessedFile is not None):
        Parser = c_parser.CParser()
        SyntaxTree = Parser.parse(PreprocessedFile, filename=FilePath)
        #SyntaxTree.show(showcoord=True)
        Visitor = StyleVisitor()
        Visitor.visit(SyntaxTree)
        Violations += Visitor.violations
        #import pprint
        #pprint.pprint(Visitor.function_list)
        #pprint.pprint(Visitor.references)
        #from pycparser import c_generator
        #generator = c_generator.CGenerator()
        #print(generator.visit(SyntaxTree))

    Result['BlankLines'] = BlankLineList
    Result['ClosingCurlies'] = ClosingCurlyList
    Result['PreprocessorStatements'] = PreprocessorList
    Result['AssertList'] = AssertList
    Result['CorrectComments'] = CorrectCommentList
    Result['SingleLineComments'] = SingleLineCommentList
    Result['MultiLineComments'] = MultiLineCommentList
    Result['ElseList'] = ElseList
    Result['WhileAfterDoList'] = WhileAfterDoList
    Result['CaseList'] = CaseList
    Result['SwitchList'] = SwitchList
    Result['DefaultList'] = DefaultList
    Result['FunctionComments'] = FunctionComments
    Result['ModuleDescription'] = ModuleDescription
    Result['Violations'] = Violations
    Result['CommentlessFile'] = CommentlessFile
    if (PreprocessedFile is not None):
        Result['SyntaxTree'] = SyntaxTree
        Result['StyleVisitor'] = Visitor
        CheckFunctions(Result)

    return Result

def GetLineFromMatch (
    String,
    MatchObject,
    UseMatchEnd=False
    ):

    if (UseMatchEnd != False):
        return String.count('\n', 0, MatchObject.end()) + 1

    return String.count('\n', 0, MatchObject.start()) + 1

class StyleVisitor (c_ast.NodeVisitor):
    def __init__(self):
        self.function_list = []
        self.current_function = None
        self.references = []
        self.violations = []

    def set_first_statement(self, n):
        if ((self.current_function is not None) and
            (self.current_function['FirstStatement'] is None) and
            (n.coord.line != 0)):

            self.current_function['FirstStatement'] = n.coord

        return

    def visit_FuncDef(self, n):
        decl = self.visit(n.decl)
        self.indent_level = 0
        PreviousFunction = self.current_function
        self.current_function = decl
        BodyBegin = self._getValidCoord(n.body)
        self.current_function['BodyBegin'] = BodyBegin
        self.current_function['BodyLength'] = \
                                       self._getLineSpan(n.body, BodyBegin) + 1

        # The body is a Compound node
        self.visit(n.body)
        self.current_function = PreviousFunction
        self.function_list.append(decl)
        return decl #+ '\n' + body + '\n'

    def visit_Decl(self, n, no_type=False):
        # no_type is used when a Decl is part of a DeclList, where the type is
        # explicitly only for the first delaration in a list.
        #
        s = n.name if no_type else self._generate_decl(n)
        #if n.bitsize: s += ' : ' + self.visit(n.bitsize)
        if (n.bitsize is not None):
            self.visit(n.bitsize)

        if n.init:
            self.visit(n.init)
            #if isinstance(n.init, c_ast.ExprList):
            #    s += ' = {' + self.visit(n.init) + '}'
            #else:
            #    s += ' = ' + self.visit(n.init)

        if (self.current_function is not None):
            self.current_function['Locals'].append(s)

        return s

    def visit_ID(self, n):
        if (self.current_function is not None):
            for Argument in self.current_function['Arguments']:
                if (Argument['Name'] == n.name):
                    return n.name

            for Local in self.current_function['Locals']:
                if (Local['Name'] == n.name):
                    return n.name

        self.references.append((self.current_function, n.name, n.coord))
        return n.name

    def visit_IdentifierType(self, n):
        return ' '.join(n.names)

    def visit_ParamList(self, n):
        Arguments = []
        for param in n.params:
            Arguments.append(self.visit(param))

        return Arguments

    def visit_If(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_For(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_While(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_DoWhile(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_Switch(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_Label(self, n):
        self.set_first_statement(n)
        NeededBlank = (n.coord.file, n.coord.line - 1)
        self.current_function['NeededBlanks'].append(
                                    (NeededBlank,
                                     VIOLATION_BLANK_LINE_NEEDED_BEFORE_LABEL))

        return self.generic_visit(n)

    def visit_Goto(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_ArrayRef(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_StructRef(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_FuncCall(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_UnaryOp(self, n):
        if n.op == 'p++':
            Violation = StyleViolation(VIOLATION_PLUS_PLUS,
                                       n.coord.file,
                                       n.coord.line)

            self.violations.append(Violation)

        elif n.op == 'p--':
            Violation = StyleViolation(VIOLATION_MINUS_MINUS,
                                       n.coord.file,
                                       n.coord.line)

            self.violations.append(Violation)

        if (n.op != 'sizeof'):
            self.set_first_statement(n)

        return self.generic_visit(n)

    def visit_BinaryOp(self, n):
        return self.generic_visit(n)

    def visit_Assignment(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_EmptyStatement(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_Return(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_TernaryOp(self, n):
        self.set_first_statement(n)
        return self.generic_visit(n)

    def visit_EllipsisParam(self, n):
        Value = {'Type': None,
                 'Name': '...',
                 'Coord': n.coord}

        return Value

    def _generate_decl(self, n):
        """ Generation from a Decl node.
        """
        return self._generate_type(n.type)

    def _generate_type(self, n, modifiers=[]):
        """ Recursive generation from a type node. n is the type node.
            modifiers collects the PtrDecl, ArrayDecl and FuncDecl modifiers
            encountered on the way down to a TypeDecl, to allow proper
            generation from it.
        """
        typ = type(n)
        if typ == c_ast.TypeDecl:
            Result = {}
            Result['Type'] = self.visit(n.type)
            Result['Coord'] = n.coord
            Result['Name'] = ''
            if (n.declname is not None):
                Result['Name'] = n.declname

            # Resolve modifiers.
            # Wrap in parens to distinguish pointer to array and pointer to
            # function syntax.
            #
            for i, modifier in enumerate(modifiers):
                if isinstance(modifier, c_ast.ArrayDecl):
                    #if (i != 0 and isinstance(modifiers[i - 1], c_ast.PtrDecl)):
                    #    nstr = '(' + nstr + ')'
                    if (modifier.dim is not None):
                        self.visit(modifier.dim)
                    #nstr += '[' + self.visit(modifier.dim) + ']'
                elif isinstance(modifier, c_ast.FuncDecl):
                    #if (i != 0 and isinstance(modifiers[i - 1], c_ast.PtrDecl)):
                    #    nstr = '(' + nstr + ')'
                    if (modifier.args is not None):
                        Arguments = self.visit(modifier.args)
                        if ((len(Arguments) == 1) and (Arguments[0] == None)):
                            Arguments = []

                        Result['Arguments'] = Arguments


                    else:
                        Result['Arguments'] = []

                    Result['Locals'] = []
                    Result['FirstStatement'] = None
                    Result['NeededBlanks'] = []

            return Result

        elif typ == c_ast.Decl:
            return self._generate_decl(n.type)
        elif typ == c_ast.Typename:
            return self._generate_type(n.type)
        elif typ == c_ast.IdentifierType:
            return ' '.join(n.names) + ' '
        elif typ in (c_ast.ArrayDecl, c_ast.PtrDecl, c_ast.FuncDecl):
            return self._generate_type(n.type, modifiers + [n])
        else:
            return self.visit(n)

    def _getLineSpan(self, node, initialCoord):
        Span = 0
        if (isinstance(node, list)):
            for Element in node:
                ChildSpan = self._getLineSpan(Element, initialCoord)
                if (ChildSpan > Span):
                    Span = ChildSpan

            return Span

        if ((node.coord is None) or (node.coord.line == 0)):
            Span = 0

        else:

            assert(node.coord.file == initialCoord.file)
            assert(node.coord.line >= initialCoord.line)

            Span = node.coord.line - initialCoord.line

        for Child in node.children():
            ChildSpan = self._getLineSpan(Child[1], initialCoord)
            if (ChildSpan > Span):
                Span = ChildSpan

        return Span

    def _getValidCoord(self, node):
        if ((node.coord is not None) and (node.coord.line != 0)):
            return node.coord

        for Child in node.children():
            ChildCoord = self._getValidCoord(Child[1])
            if (ChildCoord is not None):
                return ChildCoord

        return None

def CheckFunctions (
    ResultDictionary
    ):

    FunctionComments = ResultDictionary['FunctionComments']
    Violations = ResultDictionary['Violations']
    Visitor = ResultDictionary['StyleVisitor']
    FunctionList = Visitor.function_list

    ##
    ## Loop through every function.
    ##

    for Function in FunctionList:
        CommentBeginLine = Function['Coord'].line
        CommentEndLine = Function['BodyBegin'].line

        assert(CommentEndLine >= CommentBeginLine)

        FoundComment = None

        ##
        ## Attempt to find a function comment that is in the right spot for this
        ## function.
        ##

        for FunctionComment in FunctionComments:
            CommentLine = int(FunctionComment['LineNumber'])
            if ((CommentLine >= CommentBeginLine) and
                (CommentLine <= CommentEndLine)):

                FoundComment = FunctionComment
                break

        if (FoundComment is None):
            Violation = StyleViolation(VIOLATION_FUNCTION_COMMENT_MISSING,
                                       Function['Coord'].file,
                                       Function['Coord'].line)

            Violations.append(Violation)
            continue

        ##
        ## Ensure all parameters are documented.
        ##

        for Argument in Function['Arguments']:
            ArgumentName = Argument['Name']
            FoundArgumentComment = False
            for ArgumentComment in FoundComment['Arguments']:
                if (ArgumentComment[0] == ArgumentName):
                    FoundArgumentComment = True
                    break

            if (FoundArgumentComment == False):
                Violation = StyleViolation(VIOLATION_ARGUMENT_COMMENT_MISSING,
                                           Function['Coord'].file,
                                           Function['Coord'].line,
                                           ArgumentName)

                Violations.append(Violation)

            if (len(ArgumentName) < 2):
                Violation = StyleViolation(VIOLATION_ARGUMENT_NAME_TOO_SHORT,
                                           Argument['Coord'].file,
                                           Argument['Coord'].line,
                                           ArgumentName)

                Violations.append(Violation)

            if (ArgumentName[0:1].islower() != False):
                Violation = StyleViolation(
                                      VIOLATION_ARGUMENT_NAME_NOT_CAPTITALIZED,
                                      Argument['Coord'].file,
                                      Argument['Coord'].line,
                                      ArgumentName)

                Violations.append(Violation)

        ##
        ## Check the locals.
        ##

        FirstStatement = Function['FirstStatement'].line
        PreviousLocal = None
        for Local in Function['Locals']:
            LocalName = Local['Name']
            if (Local['Coord'].line > FirstStatement):
                Violation = StyleViolation(
                                         VIOLATION_LOCAL_NOT_AT_FUNCTION_BEGIN,
                                         Local['Coord'].file,
                                         Local['Coord'].line,
                                         LocalName)

                Violations.append(Violation)

            if (len(LocalName) < 2):
                Violation = StyleViolation(VIOLATION_LOCAL_NAME_TOO_SHORT,
                                           Local['Coord'].file,
                                           Local['Coord'].line,
                                           LocalName)

                Violations.append(Violation)

            if (LocalName[0:1].islower() != False):
                Violation = StyleViolation(
                                         VIOLATION_LOCAL_NAME_NOT_CAPTITALIZED,
                                         Local['Coord'].file,
                                         Local['Coord'].line,
                                         LocalName)

                Violations.append(Violation)

            if (PreviousLocal is not None):
                if (LocalName.lower() < PreviousLocal.lower()):
                    Violation = StyleViolation(
                                             VIOLATION_LOCALS_NOT_ALPHABETIZED,
                                             Local['Coord'].file,
                                             Local['Coord'].line,
                                             LocalName)

                    Violations.append(Violation)

            PreviousLocal = LocalName

        ##
        ## Check the blank line situation.
        ##

        CheckBlankLines(ResultDictionary, Function)

    return

def CheckBlankLines (
    ResultDictionary,
    Function,
    ):

    ##
    ## Create the list of blanks in this function.
    ##

    Violations = ResultDictionary['Violations']
    BeginLine = Function['BodyBegin'].line - 1
    EndLine = BeginLine + Function['BodyLength']
    Blanks = []
    BlanksApproved = []
    for Blank in ResultDictionary['BlankLines']:
        if ((Blank >= BeginLine) and (Blank <= EndLine + 1)):
            Blanks.append(Blank)
            BlanksApproved.append(False)

    Blanks.sort()
    CurlyList = []
    for Curly in ResultDictionary['ClosingCurlies']:
        if ((Curly >= BeginLine) and (Curly <= EndLine)):
            CurlyList.append(Curly)

    NeededBlanks = Function['NeededBlanks']

    ##
    ## Categorize each line in the function.
    ##

    InConditional = False
    InStatement = False
    StatementBegin = 0
    Lines = ResultDictionary['CommentlessFile'].split('\n')
    for LineIndex in range(BeginLine, EndLine):
        Line = Lines[LineIndex].strip()

        ##
        ## This doesn't really have to do with categorizing lines, but since
        ## we have the stripped commentless line, check here. Lone opening
        ## curlies are not allowed (it means someone put the opening curly on
        ## a different line then the if/for/etc.)
        ##

        if (Line == '{'):
            Violation = StyleViolation(VIOLATION_LONE_OPENING_CURLY,
                                       Function['Coord'].file,
                                       LineIndex + 1)

            Violations.append(Violation)

        ##
        ## Blank lines, comments, closing curlies, colons at the end (labels
        ## or cases), and preprocessor statements reset things.
        ##

        if ((Line == '') or (Line[0] == '#') or
            ((Line[0] == '/') and (Line[1] == '/')) or
            (Line == '}') or (Line[-1] == ':') or
            (LoneElseExpression.match(Line) is not None) or
            (DoExpression.match(Line) is not None)):

            InConditional = False
            InStatement = False
            continue

        ##
        ## Look for a conditional expression.
        ##

        if ((InConditional == False) and (InStatement == False)):
            ConditionalMatch = ConditionalExpression.match(Line)
            if (ConditionalMatch is not None):
                StatementBegin = LineIndex
                InConditional = True
                InStatement = False

            else:
                InConditional = False
                InStatement = True
                StatementBegin = LineIndex

        ##
        ## Try to close a conditional expression.
        ##

        if (InConditional != False):
            if ('{' in Line):
                InConditional = False
                if (LineIndex != StatementBegin):
                    Coordinate = (Function['Coord'].file, LineIndex + 2)
                    NeededBlanks.append(
                                  (Coordinate,
                                   VIOLATION_BLANK_LINE_NEEDED_AFTER_MULTILINE))

        ##
        ## Try to close a statement.
        ##

        if (InStatement != False):
            if (';' in Line):
                InStatement = False
                if (LineIndex != StatementBegin):
                    Coordinate = (Function['Coord'].file, LineIndex + 2)
                    NeededBlanks.append(
                                 (Coordinate,
                                  VIOLATION_BLANK_LINE_NEEDED_AFTER_MULTILINE))

    ##
    ## There needs to be a blank on the first line.
    ##

    if (BeginLine in Blanks):
        BlankIndex = Blanks.index(BeginLine)
        BlanksApproved[BlankIndex] = True

    else:
        Violation = StyleViolation(VIOLATION_OPENING_FUNCTION_BLANK_NEEDED,
                                   Function['Coord'].file,
                                   BeginLine)

        Violations.append(Violation)

    ##
    ## Create a violation if a required blank is missing.
    ##

    for RequiredBlank, ViolationNumber in NeededBlanks:

        ##
        ## For multi-line statement blanks, a lone curly serves as an excuse.
        ##

        CurlyExcusePresent = False
        if ((ViolationNumber == VIOLATION_BLANK_LINE_NEEDED_AFTER_MULTILINE) and
            (RequiredBlank[1] in CurlyList)):

            CurlyExcusePresent = True

        if (RequiredBlank[1] in Blanks):
            BlankIndex = Blanks.index(RequiredBlank[1])
            BlanksApproved[BlankIndex] = True

        elif (CurlyExcusePresent == False):
            Violation = StyleViolation(ViolationNumber,
                                       Function['Coord'].file,
                                       RequiredBlank[1])

            Violations.append(Violation)

    ##
    ## Ensure there's a blank line before and after any assert. Other asserts
    ## and closing curlies after are allowed as exceptions.
    ##

    AssertList = ResultDictionary['AssertList']
    for AssertLocation in AssertList:
        if ((AssertLocation[0] < BeginLine) or (AssertLocation[1]) > EndLine):
            continue

        ##
        ## Enforce the blank line before. Other asserts are allowed.
        ##

        NeededBlankLine = AssertLocation[0] - 1
        AssertBefore = False
        for OtherAssert in AssertList:
            if (OtherAssert[1] == NeededBlankLine):
                AssertBefore = True
                break

        ViolationFired = False
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        elif (AssertBefore == False):
            ViolationFired = True
            Violation = \
                StyleViolation(VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_ASSERT,
                               Function['Coord'].file,
                               NeededBlankLine + 1)

            Violations.append(Violation)

        ##
        ## Enforce the blank line after. Other asserts and lone ending curly
        ## braces serve as valid excuses.
        ##

        NeededBlankLine = AssertLocation[1] + 1
        AssertOrCurlyAfter = False
        for OtherAssert in AssertList:
            if (OtherAssert[0] == NeededBlankLine):
                AssertOrCurlyAfter = True
                break

        if (NeededBlankLine in CurlyList):
            AssertOrCurlyAfter = True

        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        elif ((AssertOrCurlyAfter == False) and (ViolationFired == False)):
            Violation = \
                StyleViolation(VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_ASSERT,
                               Function['Coord'].file,
                               NeededBlankLine - 1)

            Violations.append(Violation)

    ##
    ## Ensure there's a blank line after every lone curly. Another lone curly
    ## is an acceptable excuse.
    ##

    for Curly in CurlyList:
        if ((Curly < BeginLine) or (Curly > EndLine)):
            continue

        NeededBlankLine = Curly + 1
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        elif (not NeededBlankLine in CurlyList):
            Violation = \
                StyleViolation(VIOLATION_BLANK_LINE_NEEDED_AFTER_CLOSING_CURLY,
                               Function['Coord'].file,
                               NeededBlankLine - 1)

            Violations.append(Violation)

    ##
    ## Ensure there's a blank line before and after any well formed comment.
    ##

    for CorrectComment in ResultDictionary['CorrectComments']:
        if ((CorrectComment[0] < BeginLine) or (CorrectComment[1]) > EndLine):
            continue

        ##
        ## Enforce the blank line before.
        ##

        ViolationFired = False
        NeededBlankLine = CorrectComment[0] - 1
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        else:
            ViolationFired = True
            Violation = \
            StyleViolation(VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_COMMENT,
                           Function['Coord'].file,
                           NeededBlankLine + 1)

            Violations.append(Violation)

        ##
        ## Enforce the blank line after.
        ##

        NeededBlankLine = CorrectComment[1] + 1
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        elif (ViolationFired == False):
            Violation = \
            StyleViolation(VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_COMMENT,
                           Function['Coord'].file,
                           NeededBlankLine - 1)

            Violations.append(Violation)

    ##
    ## Ensure there's a blank line surrounding any block of preprocessor
    ## statements.
    ##

    PreprocessorList = ResultDictionary['PreprocessorStatements']
    for PreprocessorLine in PreprocessorList:
        if ((PreprocessorLine < BeginLine) or (PreprocessorLine) > EndLine):
            continue

        ##
        ## Enforce the blank line before. Other preprocessor lines are allowed.
        ##

        NeededBlankLine = PreprocessorLine - 1
        PreprocessorBefore = False
        if (NeededBlankLine in PreprocessorList):
            PreprocessorBefore = True

        ViolationFired = False
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        elif (PreprocessorBefore == False):
            ViolationFired = True
            Violation = StyleViolation(
                         VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_PREPROCESSOR,
                         Function['Coord'].file,
                         NeededBlankLine + 1)

            Violations.append(Violation)

        ##
        ## Enforce the blank line after. Other asserts and lone ending curly
        ## braces serve as valid excuses.
        ##

        NeededBlankLine = PreprocessorLine + 1
        PreprocessorAfter = False
        if (NeededBlankLine in PreprocessorList):
            PreprocessorAfter = True

        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        elif ((PreprocessorAfter == False) and (ViolationFired == False)):
            Violation = StyleViolation(
                        VIOLATION_BLANK_LINES_NEEDED_SURROUNDING_PREPROCESSOR,
                        Function['Coord'].file,
                        NeededBlankLine - 1)

            Violations.append(Violation)

    ##
    ## Enforce that any else statement has a blank line before it.
    ##

    for ElseStatement in ResultDictionary['ElseList']:
        if ((ElseStatement < BeginLine) or (ElseStatement > EndLine)):
            continue

        NeededBlankLine = ElseStatement - 1
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        else:
            Violation = StyleViolation(VIOLATION_BLANK_LINE_NEEDED_BEFORE_ELSE,
                                       Function['Coord'].file,
                                       NeededBlankLine + 1)

            Violations.append(Violation)

    ##
    ## Enforce that any while statement that concludes a do has a blank line
    ## before it.
    ##

    for WhileStatement in ResultDictionary['WhileAfterDoList']:
        if ((WhileStatement < BeginLine) or (WhileStatement > EndLine)):
            continue

        NeededBlankLine = WhileStatement - 1
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        else:
            Violation = StyleViolation(
                             VIOLATION_BLANK_LINE_NEEDED_BEFORE_WHILE_AFTER_DO,
                             Function['Coord'].file,
                             NeededBlankLine + 1)

            Violations.append(Violation)

    ##
    ## Enforce that any block of case statements has a blank line before it
    ## (except the first one in a switch).
    ##

    CaseList = ResultDictionary['CaseList']
    SwitchList = ResultDictionary['SwitchList']
    for CaseStatement in CaseList:
        if ((CaseStatement < BeginLine) or (CaseStatement > EndLine)):
            continue

        NeededBlankLine = CaseStatement - 1
        ValidExcuse = False
        if ((NeededBlankLine in CaseList) or
            (NeededBlankLine in SwitchList)):

            ValidExcuse = True

        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        elif (ValidExcuse == False):
            Violation = StyleViolation(VIOLATION_BLANK_LINE_NEEDED_BEFORE_CASE,
                                       Function['Coord'].file,
                                       NeededBlankLine + 1)

            Violations.append(Violation)

    ##
    ## Just like a case statement, a default needs a blank line before it too.
    ##

    for DefaultStatement in ResultDictionary['DefaultList']:
        if ((DefaultStatement < BeginLine) or (DefaultStatement > EndLine)):
            continue

        NeededBlankLine = DefaultStatement - 1
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        elif ((not NeededBlankLine in CaseList) and
              (not NeededBlankLine in SwitchList)):

            Violation = StyleViolation(
                                    VIOLATION_BLANK_LINE_NEEDED_BEFORE_DEFAULT,
                                    Function['Coord'].file,
                                    NeededBlankLine + 1)

            Violations.append(Violation)

    ##
    ## Enforce a blank line before the first statement. Actually, enforce that
    ## there is a first statement.
    ##

    FirstStatement = Function.get('FirstStatement')
    if (FirstStatement is None):
        Violation = StyleViolation(VIOLATION_FUNCTION_HAS_NO_STATEMENTS,
                                   Function['Coord'].file,
                                   BeginLine)

        Violations.append(Violation)

    else:
        NeededBlankLine = FirstStatement.line - 1
        if (NeededBlankLine in Blanks):
            BlankIndex = Blanks.index(NeededBlankLine)
            BlanksApproved[BlankIndex] = True

        else:
            Violation = \
                StyleViolation(VIOLATION_BLANK_LINE_NEEDED_BEFORE_FIRST_LINE,
                               Function['Coord'].file,
                               NeededBlankLine + 1)

            Violations.append(Violation)

    ##
    ## Any leftover blank lines are unnecesary.
    ##

    for BlankIndex in range(0, len(Blanks)):
        if (BlanksApproved[BlankIndex] == False):
            Violation = \
            StyleViolation(VIOLATION_UNNECESSARY_BLANK_LINE,
                           Function['Coord'].file,
                           Blanks[BlankIndex])

            Violations.append(Violation)

    return

def StripComments (
    FileContents
    ):

    InMultiLineComment = False
    Lines = FileContents.split('\n')
    for LineIndex in range(0, len(Lines)):
        Line = Lines[LineIndex]
        while (True):

            ##
            ## If already inside a multiline comment, look for the end. If
            ## found, clip the comment part and continue looking at this line.
            ## If not, clip the whole line.
            ##

            if (InMultiLineComment != False):
                CommentEndMatch = MultiLineCommentEnd.search(Line)
                if (CommentEndMatch is not None):
                    Line = Line[CommentEndMatch.end() - 2:]
                    InMultiLineComment = False

                else:
                    Line = ''
                    break

            ##
            ## If not in a multiline comment (perhaps just got out of one),
            ## then look for the beginning of a single or multiline comment.
            ##

            if (InMultiLineComment == False):
                CommentBeginMatch = MultiLineCommentBegin.search(Line)
                SingleCommentMatch = SingleLineBegin.search(Line)
                if ((SingleCommentMatch is not None) and
                    ((CommentBeginMatch is None) or
                     (CommentBeginMatch.start() > SingleCommentMatch.start()))):

                    Line = Line[:SingleCommentMatch.start() + 2]
                    break

                ##
                ## A multiline comment is beginning. If it also ends in the same
                ## line, then clip that part and continue.
                ##

                if (CommentBeginMatch is not None):
                    InMultiLineComment = True
                    CommentEndMatch = MultiLineCommentEnd.search(Line)
                    if (CommentEndMatch is not None):
                        Line = Line[:CommentBeginMatch.start()] + \
                               Line[CommentEndMatch.end():]

                        InMultiLineComment = False

                    else:
                        Line = Line[:CommentBeginMatch.start() + 2]
                        break

                else:
                    break

        Lines[LineIndex] = Line

    ##
    ## Loop through again and remove single and double quote strings.
    ##

    for LineIndex in range(0, len(Lines)):
        Line = Lines[LineIndex]
        while (True):
            SingleStringMatch = SingleString.search(Line)
            DoubleStringMatch = DoubleString.search(Line)

            ##
            ## If they both match, grab the first.
            ##

            if ((SingleStringMatch is not None) and
                (DoubleStringMatch is not None)):

                if (SingleStringMatch.start() < DoubleStringMatch.start()):
                    DoubleStringMatch = None

                else:
                    SingleStringMatch = None

            if (SingleStringMatch is not None):
                Line = Line[:SingleStringMatch.start() + 1] + \
                       Line[SingleStringMatch.end() - 1:]

            if (DoubleStringMatch is not None):
                Line = Line[:DoubleStringMatch.start() + 1] + \
                       Line[DoubleStringMatch.end() - 1:]

            if ((SingleStringMatch is None) and (DoubleStringMatch is None)):
                break

        Lines[LineIndex] = Line

    Result = '\n'.join(Lines)
    return Result

##
## Call the main function.
##

main()
