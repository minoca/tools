/*++

Copyright (c) 2015 Minoca Corp. All Rights Reserved

Module Name:

    servo.cpp

Abstract:

    This module implements the utility to enable the debug UART (UART2) on the
    servo board.

Author:

    Chris Stevens 10-Jul-2015

Environment:

    Win32

--*/

//
// ------------------------------------------------------------------- Includes
//

#include <windows.h>
#include <stdio.h>
#include "FTCI2C.h"

//
// ---------------------------------------------------------------- Definitions
//

//
// Define the clock divisor to get 400KHz.
//

#define TCA9555_400KHZ_DIVISOR 14

//
// Define the register values.
//

#define TCA9555_REGISTER_INPUT_PORT_0     0x0
#define TCA9555_REGISTER_INPUT_PORT_1     0x1
#define TCA9555_REGISTER_OUTPUT_PORT_0    0x2
#define TCA9555_REGISTER_OUTPUT_PORT_1    0x3
#define TCA9555_REGISTER_DIRECTION_PORT_0 0x6
#define TCA9555_REGISTER_DIRECTION_PORT_1 0x7

//
// Define the addresses for certain bits.
//

#define SERVO_UART2_ENABLE_ADDRESS 0x24
#define SERVO_LED_ADDRESS 0x21

//
// Define the bit to unset to enable the UART2.
//

#define TCA9555_UART2_DISABLE 0x40

//
// Define the bit to unset to enable the LED.
//

#define TCA9555_LED_DISABLE 0x08

//
// Define string value maximums.
//

#define MAX_DEVICE_NAME_LENGTH 100
#define MAX_CHANNEL_LENGTH 5
#define MAX_STATUS_ERROR_MESSAGE_LENGTH 100
#define MAX_ERROR_MESSAGE_LENGTH 200

//
// ------------------------------------------------------ Data Type Definitions
//

//
// ----------------------------------------------- Internal Function Prototypes
//

FTC_STATUS
ServoWriteRegister (
    FTC_HANDLE Handle,
    BYTE Address,
    BYTE Register,
    BYTE Value
    );

FTC_STATUS
ServoReadRegister (
    FTC_HANDLE Handle,
    BYTE Address,
    BYTE Register,
    BYTE *Value
    );

//
// -------------------------------------------------------------------- Globals
//

//
// ------------------------------------------------------------------ Functions
//

int
main (
    int ArgumentCount,
    _TCHAR* Arguments[]
    )

/*++

Routine Description:

    This routine is the main routine of the Servo board initialization
    application.

Arguments:

    ArgumentCount - Supplies the number of command line arguments the program
        was invoked with.

    Arguments - Supplies a tokenized array of command line arguments.

Return Value:

    0 on success. Non-zero on failure.

--*/

{

    char DeviceChannel[MAX_CHANNEL_LENGTH];
    DWORD DeviceCount;
    FTC_HANDLE DeviceHandle;
    DWORD DeviceIndex;
    char DeviceName[MAX_DEVICE_NAME_LENGTH];
    DWORD DeviceType;
    char ErrorMessage[MAX_ERROR_MESSAGE_LENGTH];
    FTH_INPUT_OUTPUT_PINS HighInputOutputPinsData;
    FTH_LOW_HIGH_PINS HighPinsInputData;
    DWORD LocationId;
    FTC_INPUT_OUTPUT_PINS LowInputOutputPinsData;
    FTC_LOW_HIGH_PINS LowPinsInputData;
    FTC_STATUS Status;
    char StatusErrorMessage[MAX_STATUS_ERROR_MESSAGE_LENGTH];
    BYTE TimerValue;
    BYTE Value;

    DeviceHandle = 0;
    Status = FTC_SUCCESS;

    //
    // The program takes no arguments.
    //

    if (ArgumentCount != 1) {
        printf("Too many arguments supplied.\n");
        printf("Usage: %s\n", Arguments[0]);
        return 0;
    }

    //
    // Get the number of I2C devices.
    //

    DeviceCount = 0;
    Status = I2C_GetNumHiSpeedDevices(&DeviceCount);
    if ((Status != FTC_SUCCESS) || (DeviceCount == 0)) {
        printf("%s: Found no I2C devices.\n", Arguments[0]);
        return 0;
    }

    //
    // Try to find a Channel B device.
    //

    printf("Found %d I2C devices.\n", DeviceCount);
    DeviceIndex = 0;
    while (DeviceIndex < DeviceCount) {
        DeviceType = 0;
        LocationId = 0;
        Status = I2C_GetHiSpeedDeviceNameLocIDChannel(DeviceIndex,
                                                      DeviceName,
                                                      MAX_DEVICE_NAME_LENGTH,
                                                      &LocationId,
                                                      DeviceChannel,
                                                      MAX_CHANNEL_LENGTH,
                                                      &DeviceType);

        if (Status != FTC_SUCCESS) {
            DeviceIndex = DeviceIndex + 1;
            continue;
        }

        printf("Device %d: %s, %d, %s, %d\n",
               DeviceIndex,
               DeviceName,
               LocationId,
               DeviceChannel,
               DeviceType);

        DeviceIndex = DeviceIndex + 1;
        if (strcmp(DeviceChannel, "B") == 0) {
            break;
        }

        Status = FTC_DEVICE_NOT_FOUND;
    }

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    //
    // Open the device.
    //

    Status = I2C_OpenHiSpeedDevice(DeviceName,
                                   LocationId,
                                   DeviceChannel,
                                   &DeviceHandle);

    if ((Status != FTC_SUCCESS) || (DeviceHandle == 0)) {
        goto mainEnd;
    }

    printf("Opened device: %s, %d, %s, 0x%08x.\n",
           DeviceName,
           LocationId,
           DeviceChannel);

    Status = I2C_GetHiSpeedDeviceType(DeviceHandle, &DeviceType);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = I2C_InitDevice(DeviceHandle, TCA9555_400KHZ_DIVISOR);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    TimerValue = 0;
    Status = I2C_GetDeviceLatencyTimer(DeviceHandle, &TimerValue);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = I2C_SetDeviceLatencyTimer(DeviceHandle, 50);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = I2C_GetDeviceLatencyTimer(DeviceHandle, &TimerValue);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = I2C_SetDeviceLatencyTimer(DeviceHandle, 16);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = I2C_GetDeviceLatencyTimer(DeviceHandle, &TimerValue);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = I2C_SetLoopback(DeviceHandle, false);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = I2C_SetMode(DeviceHandle, FAST_MODE);
    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    LowInputOutputPinsData.bPin1InputOutputState = false;
    LowInputOutputPinsData.bPin1LowHighState = false;
    LowInputOutputPinsData.bPin2InputOutputState = true;
    LowInputOutputPinsData.bPin2LowHighState = true;
    LowInputOutputPinsData.bPin3InputOutputState = true;
    LowInputOutputPinsData.bPin3LowHighState = false;
    LowInputOutputPinsData.bPin4InputOutputState = true;
    LowInputOutputPinsData.bPin4LowHighState = true;
    if (DeviceType == FT2232H_DEVICE_TYPE) {
        HighInputOutputPinsData.bPin1InputOutputState = true;
        HighInputOutputPinsData.bPin1LowHighState = true;
        HighInputOutputPinsData.bPin2InputOutputState = true;
        HighInputOutputPinsData.bPin2LowHighState = false;
        HighInputOutputPinsData.bPin3InputOutputState = true;
        HighInputOutputPinsData.bPin3LowHighState = false;
        HighInputOutputPinsData.bPin4InputOutputState = true;
        HighInputOutputPinsData.bPin4LowHighState = true;
        HighInputOutputPinsData.bPin5InputOutputState = true;
        HighInputOutputPinsData.bPin5LowHighState = true;
        HighInputOutputPinsData.bPin6InputOutputState = true;
        HighInputOutputPinsData.bPin6LowHighState = true;
        HighInputOutputPinsData.bPin7InputOutputState = true;
        HighInputOutputPinsData.bPin7LowHighState = true;
        HighInputOutputPinsData.bPin8InputOutputState = true;
        HighInputOutputPinsData.bPin8LowHighState = true;
        Status = I2C_SetHiSpeedDeviceGPIOs(DeviceHandle,
                                           true,
                                           &LowInputOutputPinsData,
                                           true,
                                           &HighInputOutputPinsData);

    } else {
        Status = I2C_SetHiSpeedDeviceGPIOs(DeviceHandle,
                                           true,
                                           &LowInputOutputPinsData,
                                           false,
                                           &HighInputOutputPinsData);
    }

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Sleep(200);
    if (DeviceType == FT2232H_DEVICE_TYPE) {
        Status = I2C_GetHiSpeedDeviceGPIOs(DeviceHandle,
                                           true,
                                           &LowPinsInputData,
                                           true,
                                           &HighPinsInputData);

    } else {
        Status = I2C_GetHiSpeedDeviceGPIOs(DeviceHandle,
                                           true,
                                           &LowPinsInputData,
                                           false,
                                           &HighPinsInputData);
    }

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    //
    // Enable UART2. Read-modify-write the output register, read-modify-write
    // the direction register, and then make sure that the bit is not set.
    //

    Status = ServoReadRegister(DeviceHandle,
                               SERVO_UART2_ENABLE_ADDRESS,
                               TCA9555_REGISTER_OUTPUT_PORT_0,
                               &Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Value &= ~TCA9555_UART2_DISABLE;
    Status = ServoWriteRegister(DeviceHandle,
                                SERVO_UART2_ENABLE_ADDRESS,
                                TCA9555_REGISTER_OUTPUT_PORT_0,
                                Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = ServoReadRegister(DeviceHandle,
                               SERVO_UART2_ENABLE_ADDRESS,
                               TCA9555_REGISTER_DIRECTION_PORT_0,
                               &Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Value &= ~TCA9555_UART2_DISABLE;
    Status = ServoWriteRegister(DeviceHandle,
                                SERVO_UART2_ENABLE_ADDRESS,
                                TCA9555_REGISTER_DIRECTION_PORT_0,
                                Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = ServoReadRegister(DeviceHandle,
                               SERVO_UART2_ENABLE_ADDRESS,
                               TCA9555_REGISTER_INPUT_PORT_0,
                               &Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    if ((Value & TCA9555_UART2_DISABLE) != 0) {
        printf("Servo Board UART2 failed to enable.\n");
        goto mainEnd;

    } else {
        printf("Servo Board UART2 Enabled.\n");
    }

    //
    // Flip on the LED.
    //

    Status = ServoReadRegister(DeviceHandle,
                               SERVO_LED_ADDRESS,
                               TCA9555_REGISTER_OUTPUT_PORT_1,
                               &Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Value &= ~TCA9555_LED_DISABLE;
    Status = ServoWriteRegister(DeviceHandle,
                                SERVO_LED_ADDRESS,
                                TCA9555_REGISTER_OUTPUT_PORT_1,
                                Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = ServoReadRegister(DeviceHandle,
                               SERVO_LED_ADDRESS,
                               TCA9555_REGISTER_DIRECTION_PORT_1,
                               &Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Value &= ~TCA9555_LED_DISABLE;
    Status = ServoWriteRegister(DeviceHandle,
                                SERVO_LED_ADDRESS,
                                TCA9555_REGISTER_DIRECTION_PORT_1,
                                Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

    Status = ServoReadRegister(DeviceHandle,
                               SERVO_LED_ADDRESS,
                               TCA9555_REGISTER_INPUT_PORT_1,
                               &Value);

    if (Status != FTC_SUCCESS) {
        goto mainEnd;
    }

mainEnd:
    if (DeviceHandle != 0) {
        I2C_Close(DeviceHandle);
        DeviceHandle = 0;
    }

    if (Status != FTC_SUCCESS) {
        snprintf(ErrorMessage,
                 MAX_ERROR_MESSAGE_LENGTH,
                 "Status Code(%u) - ",
                 Status);

        Status = I2C_GetErrorCodeString((LPSTR)"EN",
                                        Status,
                                        StatusErrorMessage,
                                        MAX_STATUS_ERROR_MESSAGE_LENGTH);

        strcat(ErrorMessage, StatusErrorMessage);
        printf("%s: %s\n", Arguments[0], ErrorMessage);
    }

    return 0;
}

//
// --------------------------------------------------------- Internal Functions
//

FTC_STATUS
ServoWriteRegister (
    FTC_HANDLE Handle,
    BYTE Address,
    BYTE Register,
    BYTE Value
    )

/*++

Routine Description:

    This routine writes a byte value to a register.

Arguments:

    Handle - Supplies a handle to the device to write.

    Address - Supplies the address of the register to write.

    Register - Supplies the register to write.

    Value - Supplies the byte to write to the register.

Return Value:

    Status code.

--*/

{

    FTC_PAGE_WRITE_DATA PageWriteData;
    FTC_STATUS Status;
    WriteControlByteBuffer WriteControlBuffer;
    WriteDataByteBuffer WriteDataBuffer;

    //
    // Write the device address and register to the control buffer. This is a
    // write so the low bit of the address is set to 0.
    //

    WriteControlBuffer[0] = (Address << 1) | 0x0;
    WriteControlBuffer[1] = Register;
    WriteDataBuffer[0] = Value;
    Status = I2C_Write(Handle,
                       &WriteControlBuffer,
                       2,
                       true,
                       20,
                       true,
                       BYTE_READ_TYPE,
                       &WriteDataBuffer,
                       1,
                       true,
                       20,
                       &PageWriteData);

    return Status;
}

FTC_STATUS
ServoReadRegister (
    FTC_HANDLE Handle,
    BYTE Address,
    BYTE Register,
    BYTE *Value
    )

/*++

Routine Description:

    This routine read a byte value from a register.

Arguments:

    Handle - Supplies a handle to the device to read.

    Address - Supplies the address of the register to read.

    Register - Supplies the register to read.

    Value - Supplies a pointer that receives the byte read from the register.

Return Value:

    Status code.

--*/

{

    ReadDataByteBuffer ReadDataBuffer;
    FTC_STATUS Status;
    WriteControlByteBuffer WriteControlBuffer;

    //
    // Write the device address and register to the control buffer. This is a
    // read so the low bit of the address is set to 1.
    //

    WriteControlBuffer[0] = (Address << 1) | 0x1;
    WriteControlBuffer[1] = Register;
    Status = I2C_Read(Handle,
                      &WriteControlBuffer,
                      2,
                      true,
                      20,
                      BYTE_READ_TYPE,
                      &ReadDataBuffer,
                      1);

    if (Status == FTC_SUCCESS) {
        *Value = ReadDataBuffer[0];
    }

    return Status;
}

