/*
 * AM33XX-CM3 firmware
 *
 * Cortex-M3 (CM3) firmware for power management on Texas Instruments' AM33XX series of SoCs
 *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  This software is licensed under the  standard terms and conditions in the Texas Instruments  Incorporated
 *  Technology and Software Publicly Available Software License Agreement , a copy of which is included in the
 *  software download.
*/

#include <trace.h>
#include <msg.h>

/* TODO: ROM code ate up the trace location. Can we manage without this? */
void trace_init(void)
{

}

void trace_update(void)
{

}

/* Use bit-banding here */
void trace_get_current_pos(void)
{

}

/* Intended to be called in case of errors/exceptions */
void trace_set_current_pos(void)
{

}

void trace_cmd(int value) {
    int reg = msg_read(CUST_REG);
    reg &= ~0x000000FF;
    reg |= (value & 0xFF);
    msg_write(reg, CUST_REG);
    return;
}

void trace_wake(int value) {
    int reg = msg_read(CUST_REG);
    reg &= ~0x0000FF00;
    reg |= (value << 8) & 0x0000FF00;
    msg_write(reg, CUST_REG);
    return;
}

