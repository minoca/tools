##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved
##
## Script Name:
##
##     start-env.sh
##
## Abstract:
##
##     This script fires up the power meter environment.
##
## Author:
##
##     Evan Green 3-Nov-2015
##
## Environment:
##
##     sh (swiss) on Windows
##

if test -z "$VIRTUALENV"; then
  . $SRCROOT/tools/resources/pmeter/venv/Scripts/activate
fi

export TCL_LIBRARY="$SRCROOT/tools/win32/ppython/App/tcl/tcl8.5/"
export TK_LIBRARY="$SRCROOT/tools/win32/ppython/App/tcl/tk8.5/"

echo "Done activating dev environment."
