##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved
##
## Script Name:
##
##     setup-win.sh
##
## Abstract:
##
##     This script sets up the power meter environment on Windows.
##
## Author:
##
##     Evan Green 3-Nov-2015
##
## Environment:
##
##     sh (swiss) on Windows
##

set -x

SRCROOT=`echo $SRCROOT | sed 's_\\\\_/_g'`

##
## Exit the virtualenv if in one, as PATH often gets reset by a new
## invocation of sh.
##

unset VIRTUAL_ENV || true

##
## Enter the virtualenv if not in it.
##

if test -z "$VIRTUAL_ENV"; then

  ##
  ## Create the VENV if it does not exist.
  ##

  VENV=$SRCROOT/tools/resources/pmeter/venv
  if ! test -d "$VENV"; then
    virtualenv "$VENV"
    ACTIVATE=$VENV/Scripts/activate

    ##
    ## The sed script replaces the VIRTUALENV=... line with the correct path.
    ## It also replaces incorrect PATH separator in the appending of the PATH.
    ##

    SEDSCRIPT="s+VIRTUAL_ENV=\"\$(if.*+VIRTUAL_ENV='$VENV'+"
    SEDSCRIPT="${SEDSCRIPT};s+PATH=\"\$VIRTUAL_ENV/Scripts:\$PATH\"+PATH=\"\$VIRTUAL_ENV/Scripts;\$PATH\"+"
    echo $SEDSCRIPT
    sed "$SEDSCRIPT" $ACTIVATE > ${ACTIVATE}2
    mv "${ACTIVATE}2" "$ACTIVATE"
  fi

  if ! test -d "$VENV"; then
    echo "Error: $VENV doesnt' exist after creating virtualenv."
    exit 1
  fi

  . $VENV/Scripts/activate
fi

if test -z "$VIRTUAL_ENV"; then
  echo "Error: Failed to enter virtualenv."
  exit 1
fi

##
## Install package dependencies.
##

pip install pytz==2015.7 six==1.10.0 python-dateutil==2.4.2 pyparsing==2.0.5 cycler==0.9.0
pip install $SRCROOT/tools/resources/pmeter/pylibs/numpy-1.10.1+mkl-cp27-none-win32.whl
pip install $SRCROOT/tools/resources/pmeter/pylibs/matplotlib-1.5.0-cp27-none-win32.whl

echo "Completed setup. Now run '. start-env.sh' to enable the environment."

