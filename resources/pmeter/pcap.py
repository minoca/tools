from __future__ import print_function
from collections import deque
import csv
import os
import sys
from time import sleep
import time

def print_error(*objs):
    print(*objs, file=sys.stderr)
    
class PowerMeterCapture():
    def __init__(self, input, output):
        self.input = input
        self.output = output
        self.offset = 0
        self.overflow = 0
        self.prev_tstamp = 0
        
    def run(self):
        current_time = time.strftime("%m/%d/%Y %H:%M:%S")
        print_error("Beginning trace at %s" % current_time)
        self.output.write("%s\nseconds,current\n" % current_time)
        incsv = csv.reader(self.input)
        outcsv = csv.writer(self.output)
        stdoutcsv = csv.writer(sys.stdout)
        while True:
            try:
                line = incsv.next()
                if len(line) < 2:
                    continue
                    
                tstamp = int(line[0], 16)
                if self.offset == 0:
                    self.offset = tstamp
                    
                tstamp -= self.offset
                if tstamp < self.prev_tstamp:
                    self.overflow += 0x100000000
                    
                self.prev_tstamp = tstamp
                tstamp += self.overflow
                tstamp = tstamp / 2000000.0
                milliamps = int(line[1]) / 1000.0
                datapoint = [tstamp, milliamps]
                outcsv.writerow(datapoint)
                stdoutcsv.writerow(datapoint)
                
            except (ValueError, KeyboardInterrupt, IOError):
                break

        return
    
def main():
    if len(sys.argv) != 2:
        print("pcap.py captures raw power meter input, formats it, and feeds ")
        print("it both to stdout and a file.")
        print("Usage: pgraph.py COM7")
        return 1
    
    path = sys.argv[1]
    if path[:3].lower() == 'com':
        path = '\\\\.\\' + path
        
    input = open(path, 'r')
    input.readline()
    for i in range(0, 100):
        output_path = 'trace%d.csv' % i
        if not os.path.exists(output_path):
            output = open(output_path, 'wb')
            print_error("Output %s" % output_path)
            break
            
    capture = PowerMeterCapture(input, output)
    capture.run()
    input.close()
    output.close()
    try:
        sys.stdout.flush()
        
    except IOError:
        pass
        
    return 0
    
if __name__ == "__main__":
    exit(main())