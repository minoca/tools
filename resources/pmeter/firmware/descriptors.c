/*++

Copyright (c) 2015 Minoca Corp. All Rights Reserved

Module Name:

    descriptors.c

Abstract:

    This module contains the USB descriptors for the power meter firmware.

Author:

    Evan Green 3-Nov-2015

Environment:

    AVR Firmware

--*/

//
// ------------------------------------------------------------------- Includes
//

#include "Descriptors.h"

//
// ---------------------------------------------------------------- Definitions
//

//
// ------------------------------------------------------ Data Type Definitions
//

//
// ----------------------------------------------- Internal Function Prototypes
//

//
// -------------------------------------------------------------------- Globals
//

//
// Define the main USB device descriptor.
//

const USB_Descriptor_Device_t PROGMEM DeviceDescriptor = {
    .Header = {
        .Size = sizeof(USB_Descriptor_Device_t),
        .Type = DTYPE_Device
    },

    .USBSpecification = VERSION_BCD(1,1,0),
    .Class = CDC_CSCP_CDCClass,
    .SubClass = CDC_CSCP_NoSpecificSubclass,
    .Protocol = CDC_CSCP_NoSpecificProtocol,
    .Endpoint0Size = FIXED_CONTROL_ENDPOINT_SIZE,
    .VendorID = 0x8619,
    .ProductID = 0x0701,
    .ReleaseNumber = VERSION_BCD(0,0,1),
    .ManufacturerStrIndex = STRING_ID_Manufacturer,
    .ProductStrIndex = STRING_ID_Product,
    .SerialNumStrIndex = USE_INTERNAL_SERIAL,
    .NumberOfConfigurations = FIXED_NUM_CONFIGURATIONS
};

//
// Define the configuration descriptor.
//

const USB_Descriptor_Configuration_t PROGMEM ConfigurationDescriptor = {
    .Config = {
        .Header = {
            .Size = sizeof(USB_Descriptor_Configuration_Header_t),
            .Type = DTYPE_Configuration
        },

        .TotalConfigurationSize = sizeof(USB_Descriptor_Configuration_t),
        .TotalInterfaces = 2,
        .ConfigurationNumber = 1,
        .ConfigurationStrIndex = NO_DESCRIPTOR,
        .ConfigAttributes = (USB_CONFIG_ATTR_RESERVED |
                             USB_CONFIG_ATTR_SELFPOWERED),

        .MaxPowerConsumption = USB_CONFIG_POWER_MA(100)
    },

    .CDC_CCI_Interface = {
        .Header = {
            .Size = sizeof(USB_Descriptor_Interface_t),
            .Type = DTYPE_Interface
        },

        .InterfaceNumber = INTERFACE_ID_CDC_CCI,
        .AlternateSetting = 0,
        .TotalEndpoints = 1,
        .Class = CDC_CSCP_CDCClass,
        .SubClass = CDC_CSCP_ACMSubclass,
        .Protocol = CDC_CSCP_ATCommandProtocol,
        .InterfaceStrIndex = NO_DESCRIPTOR
    },

    .CDC_Functional_Header = {
        .Header = {
            .Size = sizeof(USB_CDC_Descriptor_FunctionalHeader_t),
            .Type = DTYPE_CSInterface
        },

        .Subtype = CDC_DSUBTYPE_CSInterface_Header,
        .CDCSpecification = VERSION_BCD(1,1,0),
    },

    .CDC_Functional_ACM = {
        .Header = {
            .Size = sizeof(USB_CDC_Descriptor_FunctionalACM_t),
            .Type = DTYPE_CSInterface
        },

        .Subtype = CDC_DSUBTYPE_CSInterface_ACM,
        .Capabilities = 0x06,
    },

    .CDC_Functional_Union = {
        .Header = {
            .Size = sizeof(USB_CDC_Descriptor_FunctionalUnion_t),
            .Type = DTYPE_CSInterface
        },

        .Subtype = CDC_DSUBTYPE_CSInterface_Union,
        .MasterInterfaceNumber = INTERFACE_ID_CDC_CCI,
        .SlaveInterfaceNumber = INTERFACE_ID_CDC_DCI,
    },

    .CDC_NotificationEndpoint = {
        .Header = {
            .Size = sizeof(USB_Descriptor_Endpoint_t),
            .Type = DTYPE_Endpoint
        },

        .EndpointAddress = CDC_NOTIFICATION_EPADDR,
        .Attributes = (EP_TYPE_INTERRUPT | ENDPOINT_ATTR_NO_SYNC |
                       ENDPOINT_USAGE_DATA),

        .EndpointSize = CDC_NOTIFICATION_EPSIZE,
        .PollingIntervalMS = 0xFF
    },

    .CDC_DCI_Interface = {
        .Header = {
            .Size = sizeof(USB_Descriptor_Interface_t),
            .Type = DTYPE_Interface
        },

        .InterfaceNumber = INTERFACE_ID_CDC_DCI,
        .AlternateSetting = 0,
        .TotalEndpoints = 2,
        .Class = CDC_CSCP_CDCDataClass,
        .SubClass = CDC_CSCP_NoDataSubclass,
        .Protocol = CDC_CSCP_NoDataProtocol,
        .InterfaceStrIndex = NO_DESCRIPTOR
    },

    .CDC_DataOutEndpoint = {
        .Header = {
            .Size = sizeof(USB_Descriptor_Endpoint_t),
            .Type = DTYPE_Endpoint
        },

        .EndpointAddress = CDC_RX_EPADDR,
        .Attributes = (EP_TYPE_BULK | ENDPOINT_ATTR_NO_SYNC |
                       ENDPOINT_USAGE_DATA),

        .EndpointSize = CDC_TXRX_EPSIZE,
        .PollingIntervalMS = 0x05
    },

    .CDC_DataInEndpoint = {
        .Header = {
            .Size = sizeof(USB_Descriptor_Endpoint_t),
            .Type = DTYPE_Endpoint
        },

        .EndpointAddress = CDC_TX_EPADDR,
        .Attributes = (EP_TYPE_BULK | ENDPOINT_ATTR_NO_SYNC |
                       ENDPOINT_USAGE_DATA),

        .EndpointSize = CDC_TXRX_EPSIZE,
        .PollingIntervalMS = 0x05
    }
};

//
// Define the array of supported languages.
//

const USB_Descriptor_String_t PROGMEM LanguageString =
    USB_STRING_DESCRIPTOR_ARRAY(LANGUAGE_ID_ENG);

//
// Define the manufacturer.
//

const USB_Descriptor_String_t PROGMEM ManufacturerString =
    USB_STRING_DESCRIPTOR(L"Minoca");

//
// Define the product name string.
//

const USB_Descriptor_String_t PROGMEM ProductString =
    USB_STRING_DESCRIPTOR(L"Minoca Power Meter");

//
// ------------------------------------------------------------------ Functions
//

uint16_t
CALLBACK_USB_GetDescriptor (
    const uint16_t Value,
    const uint8_t Index,
    const void **const DescriptorAddress
    )

/*++

Routine Description:

    This routine is called by the USB library when in device mode to get a
    USB descriptor.

Arguments:

    Value - Supplies the value portion of the control request for the
        descriptor (the descriptor type).

    Index - Supplies the index from the control request.

    DescriptorAddress - Supplies a pointer where a pointer to the descriptor
        data will be returned on success.

Return Value:

    Returns the size of the descriptor returned.
--*/

{
    uint8_t DescriptorType;
    uint8_t DescriptorNumber;
    const void *Address;
    uint16_t Size;

    DescriptorType = (Value >> 8);
    DescriptorNumber = (Value & 0xFF);
    Address = NULL;
    Size = NO_DESCRIPTOR;
    switch (DescriptorType) {
    case DTYPE_Device:
        Address = &DeviceDescriptor;
        Size = sizeof(USB_Descriptor_Device_t);
        break;

    case DTYPE_Configuration:
        Address = &ConfigurationDescriptor;
        Size = sizeof(USB_Descriptor_Configuration_t);
        break;

    case DTYPE_String:
        switch (DescriptorNumber) {
        case STRING_ID_Language:
            Address = &LanguageString;
            Size = pgm_read_byte(&LanguageString.Header.Size);
            break;

        case STRING_ID_Manufacturer:
            Address = &ManufacturerString;
            Size = pgm_read_byte(&ManufacturerString.Header.Size);
            break;
        case STRING_ID_Product:
            Address = &ProductString;
            Size = pgm_read_byte(&ProductString.Header.Size);
            break;
        }

        break;
    }

    *DescriptorAddress = Address;
    return Size;
}

//
// --------------------------------------------------------- Internal Functions
//

