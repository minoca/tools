/*++

Copyright (c) 2015 Minoca Corp. All Rights Reserved

Module Name:

    ina219.h

Abstract:

    This header contains definitions for the INA-219 DC current sensor breakout
    board from Adafruit.

Author:

    Evan Green 3-Nov-2015

--*/

//
// ------------------------------------------------------------------- Includes
//

//
// ---------------------------------------------------------------- Definitions
//

#define INA219_ADDRESS 0x80
#define INA219_READ 0x01

//
// Configuration register
//

#define INA219_REG_CONFIG                      0x00

#define INA219_CONFIG_RESET                    0x8000

//
// Bus voltage range mask: 0-16V or 0-32V range
//

#define INA219_CONFIG_BVOLTAGERANGE_MASK       0x2000
#define INA219_CONFIG_BVOLTAGERANGE_16V        0x0000
#define INA219_CONFIG_BVOLTAGERANGE_32V        0x2000

//
// Voltage gain setting: millivolt range.
//

#define INA219_CONFIG_GAIN_MASK                0x1800
#define INA219_CONFIG_GAIN_1_40MV              0x0000
#define INA219_CONFIG_GAIN_2_80MV              0x0800
#define INA219_CONFIG_GAIN_4_160MV             0x1000
#define INA219_CONFIG_GAIN_8_320MV             0x1800

//
// Bus ADC resolution bit count.
//

#define INA219_CONFIG_BADCRES_MASK             0x0780
#define INA219_CONFIG_BADCRES_9BIT             0x0080
#define INA219_CONFIG_BADCRES_10BIT            0x0100
#define INA219_CONFIG_BADCRES_11BIT            0x0200
#define INA219_CONFIG_BADCRES_12BIT            0x0400

//
// Shunt ADC resolution and averaging mask: number of samples averaged
// together and duration. For example, 8S_4260US is 8 samples averaged over
// the course of 4260 microseconds.
//

#define INA219_CONFIG_SADCRES_MASK             0x0078
#define INA219_CONFIG_SADCRES_9BIT_1S_84US     0x0000
#define INA219_CONFIG_SADCRES_10BIT_1S_148US   0x0008
#define INA219_CONFIG_SADCRES_11BIT_1S_276US   0x0010
#define INA219_CONFIG_SADCRES_12BIT_1S_532US   0x0018
#define INA219_CONFIG_SADCRES_12BIT_2S_1060US  0x0048
#define INA219_CONFIG_SADCRES_12BIT_4S_2130US  0x0050
#define INA219_CONFIG_SADCRES_12BIT_8S_4260US  0x0058
#define INA219_CONFIG_SADCRES_12BIT_16S_8510US 0x0060
#define INA219_CONFIG_SADCRES_12BIT_32S_17MS   0x0068
#define INA219_CONFIG_SADCRES_12BIT_64S_34MS   0x0070
#define INA219_CONFIG_SADCRES_12BIT_128S_69MS  0x0078

//
// Operating modes.
//

#define INA219_CONFIG_MODE_MASK                 0x0007
#define INA219_CONFIG_MODE_POWERDOWN            0x0000
#define INA219_CONFIG_MODE_SVOLT_TRIGGERED      0x0001
#define INA219_CONFIG_MODE_BVOLT_TRIGGERED      0x0002
#define INA219_CONFIG_MODE_SANDBVOLT_TRIGGERED  0x0003
#define INA219_CONFIG_MODE_ADCOFF               0x0004
#define INA219_CONFIG_MODE_SVOLT_CONTINUOUS     0x0005
#define INA219_CONFIG_MODE_BVOLT_CONTINUOUS     0x0006
#define INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS 0x0007

//
// Other registers.
//

#define INA219_REG_SHUNTVOLTAGE                0x01
#define INA219_REG_BUSVOLTAGE                  0x02
#define INA219_REG_POWER                       0x03
#define INA219_REG_CURRENT                     0x04
#define INA219_REG_CALIBRATION                 0x05

//
// ------------------------------------------------------ Data Type Definitions
//

/*++

Structure Description:

    This structure stores the context for an INA-219 DC current sensor.

Members:

    SlaveAddress - Stores the I2C slave address of the device.

    Calibration - Stores the calibration register value.

    CurrentDividerMilliamps - Stores the multiplier to convert raw current
        values into milliamps.

    PowerDividerMilliwatts - Stores the multiplier to convert raw power values
        to milliwatts.

--*/

typedef struct _INA219 {
    uint8_t SlaveAddress;
    uint32_t Calibration;
    uint32_t CurrentDividerMilliamps;
    uint32_t PowerDividerMilliwatts;
} INA219, *PINA219;

//
// -------------------------------------------------------------------- Globals
//

//
// -------------------------------------------------------- Function Prototypes
//

void
Ina219Initialize (
    uint8_t SlaveAddress,
    PINA219 Device
    );

/*++

Routine Description:

    This routine initializes the INA-219 device context.

Arguments:

    SlaveAddress - Supplies the address the slave communicates on.

    Device - Supplies a pointer to the device context.

Return Value:

    None.

--*/

void
Ina219Calibrate32V2A (
    PINA219 Device
    );

/*++

Routine Description:

    This routine calibrates the INA-219 for a range of 32 Volts and 2 Amps.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    None.

--*/

void
Ina219Calibrate32V1A (
    PINA219 Device
    );

/*++

Routine Description:

    This routine calibrates the INA-219 for a range of 32 Volts and 1 Amp.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    None.

--*/

void
Ina219Calibrate16V400ma (
    PINA219 Device
    );

/*++

Routine Description:

    This routine calibrates the INA-219 for a range of 16 Volts and 400
    milliamps.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    None.

--*/

int16_t
Ina219GetBusVoltage (
    PINA219 Device
    );

/*++

Routine Description:

    This routine returns the bus voltage of the INA-219 sensor.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    Returns the bus voltage in millivolts.

--*/

int16_t
Ina219GetShuntVoltage (
    PINA219 Device
    );

/*++

Routine Description:

    This routine returns the shunt voltage of the INA-219 sensor.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    Returns the shunt voltage in millivolts.

--*/

int32_t
Ina219GetCurrent (
    PINA219 Device
    );

/*++

Routine Description:

    This routine returns the current flowing through the INA-219 sensor.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    Returns the current in microamps.

--*/
