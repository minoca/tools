/*++

Copyright (c) 2015 Minoca Corp. All Rights Reserved

Module Name:

    ina219.c

Abstract:

    This module implements support for the INA219 DC current sensor breakout
    board from Adafruit.

Author:

    Evan Green 3-Nov-2015

Environment:

    AVR firmware

--*/

//
// ------------------------------------------------------------------- Includes
//

#include "pmeter.h"

//
// ---------------------------------------------------------------- Definitions
//

//
// ------------------------------------------------------ Data Type Definitions
//

//
// ----------------------------------------------- Internal Function Prototypes
//

void
Ina219Write (
    uint8_t SlaveAddress,
    uint8_t Register,
    uint16_t Value
    );

void
Ina219Read (
    uint8_t SlaveAddress,
    uint8_t Register,
    uint16_t *Value
    );

//
// -------------------------------------------------------------------- Globals
//

//
// ------------------------------------------------------------------ Functions
//

void
Ina219Initialize (
    uint8_t SlaveAddress,
    PINA219 Device
    )

/*++

Routine Description:

    This routine initializes the INA-219 device context.

Arguments:

    SlaveAddress - Supplies the address the slave communicates on.

    Device - Supplies a pointer to the device context.

Return Value:

    None.

--*/

{

    Device->SlaveAddress = SlaveAddress;
    Device->Calibration = 0;
    Device->CurrentDividerMilliamps = 0;
    Device->PowerDividerMilliwatts = 0;
    return;
}

void
Ina219Calibrate32V2A (
    PINA219 Device
    )

/*++

Routine Description:

    This routine calibrates the INA-219 for a range of 32 Volts and 2 Amps.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    None.

--*/

{

    uint16_t Config;

    //
    // VBUS_MAX = 32V (Can also be set to 16V).
    // VSHUNT_MAX = 0.32 (Uses Gain 8, 320mV, can also be 0.16, 0.08, or 0.04).
    // RSHUNT = 0.1 (Resistor value in Ohms).
    //
    // Determine max current: MaxI = VSHUNT_MAX / RSHUNT = 3.2A.
    // Determine max expected current: MaxExpectedI = 2.0A.
    // Calculate range of LSB (minimum 15-bit maximum 12-bit):
    //   MinLSB = MaxExpectedI / 32767 = 0.000061 (61uA per bit).
    //   MaxLSB = MaxExpectedI / 4096 = 0.000488 (488uA per bit).
    // Choose an LSB between the min and the max values:
    //   CurrentLSB = 0.0001 (100uA per bit).
    // Compute the calibration register:
    //   Cal = trunc(0.04096 / (CurrentLSB * RSHUNT)) = 4096.
    //

    Device->Calibration = 4096;

    //
    // Calculate the power LSB: PowerLSB = 20 * CurrentLSB = 0.002 (2mW per
    // bit).
    // Compute the maximum current and shunt voltage before overflow:
    // MaxCurrent = CurrentLSB * 32767 = 3.2767A before overflow.
    // MaxCurrentBeforeOverflow = min(MaxI, MaxCurrent).
    // MaxShuntVoltage = MaxCurrentBeforeOverflow * RSHUNT = 0.32V.
    // MaxShuntVoltageBeforeOverflow = min(VSHUNT_MAX, MaxShuntVoltage).
    // MaximumPower = MaxCurrentBeforeOverflow * VBUS_MAX = 3.2 * 32V = 102.4W.
    //
    // CurrentLSB = 100uA per bit (1000/10).
    // PowerLSB = 1mW per bit (2/1).
    //
    //

    Device->CurrentDividerMilliamps = 10;
    Device->PowerDividerMilliwatts = 2;
    Ina219Write(Device->SlaveAddress,
                INA219_REG_CALIBRATION,
                Device->Calibration);

    Config = INA219_CONFIG_BVOLTAGERANGE_32V |
             INA219_CONFIG_GAIN_8_320MV |
             INA219_CONFIG_BADCRES_12BIT |
             INA219_CONFIG_SADCRES_12BIT_1S_532US |
             INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS;

    Ina219Write(Device->SlaveAddress, INA219_REG_CONFIG, Config);
    return;
}

void
Ina219Calibrate32V1A (
    PINA219 Device
    )

/*++

Routine Description:

    This routine calibrates the INA-219 for a range of 32 Volts and 1 Amp.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    None.

--*/

{

    uint16_t Config;

    //
    // VBUS_MAX = 32V (Can also be set to 16V).
    // VSHUNT_MAX = 0.32 (Uses Gain 8, 320mV, can also be 0.16, 0.08, or 0.04).
    // RSHUNT = 0.1 (Resistor value in Ohms).
    //
    // Determine max current: MaxI = VSHUNT_MAX / RSHUNT = 3.2A.
    // Determine max expected current: MaxExpectedI = 1.0A.
    // Calculate range of LSB (minimum 15-bit maximum 12-bit):
    //   MinLSB = MaxExpectedI / 32767 = 0.0000305 (30.5uA per bit).
    //   MaxLSB = MaxExpectedI / 4096 = 0.000244 (244uA per bit).
    // Choose an LSB between the min and the max values:
    //   CurrentLSB = 0.0004 (400uA per bit).
    // Compute the calibration register:
    //   Cal = trunc(0.04096 / (CurrentLSB * RSHUNT)) = 10240.
    //

    Device->Calibration = 10240;

    //
    // Calculate the power LSB: PowerLSB = 20 * CurrentLSB = 0.0008 (800uW per
    // bit).
    // Compute the maximum current and shunt voltage before overflow:
    // MaxCurrent = CurrentLSB * 32767 = 1.31068A before overflow.
    // MaxCurrentBeforeOverflow = min(MaxI, MaxCurrent).
    // MaxShuntVoltage = MaxCurrentBeforeOverflow * RSHUNT = 0.131068V.
    // MaxShuntVoltageBeforeOverflow = min(VSHUNT_MAX, MaxShuntVoltage).
    // MaximumPower = MaxCurrentBeforeOverflow * VBUS_MAX = 1.31068 * 32V
    //              = 41.94176W.
    //
    // CurrentLSB = 40uA per bit (1000/25).
    // PowerLSB = 800uW per bit (800/1).
    //
    //

    Device->CurrentDividerMilliamps = 25;
    Device->PowerDividerMilliwatts = 1;
    Ina219Write(Device->SlaveAddress,
                INA219_REG_CALIBRATION,
                Device->Calibration);

    Config = INA219_CONFIG_BVOLTAGERANGE_32V |
             INA219_CONFIG_GAIN_8_320MV |
             INA219_CONFIG_BADCRES_12BIT |
             INA219_CONFIG_SADCRES_12BIT_1S_532US |
             INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS;

    Ina219Write(Device->SlaveAddress, INA219_REG_CONFIG, Config);
    return;
}

void
Ina219Calibrate16V400ma (
    PINA219 Device
    )

/*++

Routine Description:

    This routine calibrates the INA-219 for a range of 16 Volts and 400
    milliamps.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    None.

--*/

{

    uint16_t Config;

    //
    // VBUS_MAX = 16V.
    // VSHUNT_MAX = 0.04 (Uses Gain 1, 40mV).
    // RSHUNT = 0.1 (Resistor value in Ohms).
    //
    // Determine max current: MaxI = VSHUNT_MAX / RSHUNT = 0.4A.
    // Determine max expected current: MaxExpectedI = 0.4A.
    // Calculate range of LSB (minimum 15-bit maximum 12-bit):
    //   MinLSB = MaxExpectedI / 32767 = 0.0000122 (12.2uA per bit).
    //   MaxLSB = MaxExpectedI / 4096 = 0.0000977 (97.7uA per bit).
    // Choose an LSB between the min and the max values:
    //   CurrentLSB = 0.00005 (50uA per bit).
    // Compute the calibration register:
    //   Cal = trunc(0.04096 / (CurrentLSB * RSHUNT)) = 8192.
    //

    Device->Calibration = 8192;

    //
    // Calculate the power LSB: PowerLSB = 20 * CurrentLSB = 0.001 (1mW per
    // bit).
    // Compute the maximum current and shunt voltage before overflow:
    // MaxCurrent = CurrentLSB * 32767 = 1.63835A before overflow.
    // MaxCurrentBeforeOverflow = min(MaxI, MaxCurrent).
    // MaxShuntVoltage = MaxCurrentBeforeOverflow * RSHUNT = 0.04V.
    // MaxShuntVoltageBeforeOverflow = min(VSHUNT_MAX, MaxShuntVoltage).
    // MaximumPower = MaxCurrentBeforeOverflow * VBUS_MAX = 0.4 * 16V
    //              = 6.4W
    //
    // CurrentLSB = 50uA per bit (1000/50).
    // PowerLSB = 1mW per bit (1000/1).
    //
    //

    Device->CurrentDividerMilliamps = 20;
    Device->PowerDividerMilliwatts = 1;
    Ina219Write(Device->SlaveAddress,
                INA219_REG_CALIBRATION,
                Device->Calibration);

    Config = INA219_CONFIG_BVOLTAGERANGE_16V |
             INA219_CONFIG_GAIN_1_40MV |
             INA219_CONFIG_BADCRES_12BIT |
             INA219_CONFIG_SADCRES_12BIT_1S_532US |
             INA219_CONFIG_MODE_SANDBVOLT_CONTINUOUS;

    Ina219Write(Device->SlaveAddress, INA219_REG_CONFIG, Config);
    return;
}

int16_t
Ina219GetBusVoltage (
    PINA219 Device
    )

/*++

Routine Description:

    This routine returns the bus voltage of the INA-219 sensor.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    Returns the bus voltage in millivolts.

--*/

{

    uint16_t Value;

    Ina219Read(Device->SlaveAddress, INA219_REG_BUSVOLTAGE, &Value);

    //
    // Shift off the CNVR and OVR bits and multiply by the least significant
    // bit.
    //

    return (int16_t)((Value >> 3) * 4);
}

int16_t
Ina219GetShuntVoltage (
    PINA219 Device
    )

/*++

Routine Description:

    This routine returns the shunt voltage of the INA-219 sensor.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    Returns the shunt voltage in microvolts * 10.

--*/

{

    uint16_t Value;

    Ina219Read(Device->SlaveAddress, INA219_REG_SHUNTVOLTAGE, &Value);
    return (int16_t)Value;
}

int32_t
Ina219GetCurrent (
    PINA219 Device
    )

/*++

Routine Description:

    This routine returns the current flowing through the INA-219 sensor.

Arguments:

    Device - Supplies a pointer to the device context.

Return Value:

    Returns the current in microamps.

--*/

{

    int32_t Microamps;
    uint16_t Value;

    //
    // References indicate that the calibration register may reset unexpectedly
    // and need to be reprogrammed. Add this in if this turns out to be true:
    // Ina219Write(Device->SlaveAddress,
    //             INA219_REG_CALIBRATION,
    //             Device->Calibration);

    Ina219Read(Device->SlaveAddress, INA219_REG_CURRENT, &Value);

    //
    // Shift off the CNVR and OVR bits and multiply by the least significant
    // bit.
    //

    Microamps = (((int32_t)Value) * 1000L) /
                Device->CurrentDividerMilliamps;

    return Microamps;
}

//
// --------------------------------------------------------- Internal Functions
//

void
Ina219Write (
    uint8_t SlaveAddress,
    uint8_t Register,
    uint16_t Value
    )

/*++

Routine Description:

    This routine writes to an INA-219 register.

Arguments:

    SlaveAddress - Supplies the slave address of the device.

    Register - Supplies the register to write.

    Value - Supplies the value to write.

Return Value:

    None.

--*/

{

    I2cStart();
    I2cWrite(SlaveAddress);
    I2cWrite(Register);
    I2cWrite((Value >> 8) & 0xFF);
    I2cWrite(Value & 0xFF);
    I2cStop();
    return;
}

void
Ina219Read (
    uint8_t SlaveAddress,
    uint8_t Register,
    uint16_t *Value
    )

/*++

Routine Description:

    This routine read reads from an INA-219 register.

Arguments:

    SlaveAddress - Supplies the slave address of the device.

    Register - Supplies the register to read from.

    Value - Supplies a pointer where the value will be returned on success.

Return Value:

    None.

--*/

{

    uint8_t Data[2];

    I2cStart();
    I2cWrite(SlaveAddress);
    I2cWrite(Register);
    I2cStart();
    I2cWrite(SlaveAddress | INA219_READ);
    I2cReadMultiple(Data, 2);
    *Value = ((uint16_t)(Data[0]) << 8) | Data[1];
    I2cStop();
    return;
}

