/*++

Copyright (c) 2015 Minoca Corp. All Rights Reserved

Module Name:

    descriptors.h

Abstract:

    This header contains descriptor-related definitions for the power meter
    firmware.

Author:

    Evan Green 3-Nov-2015

--*/

//
// ------------------------------------------------------------------- Includes
//

#include <avr/pgmspace.h>
#include <LUFA/Drivers/USB/USB.h>

//
// ---------------------------------------------------------------- Definitions
//

//
// Define the endpoint address of the CDC device to host notification IN
// endpoint.
//

#define CDC_NOTIFICATION_EPADDR (ENDPOINT_DIR_IN | 2)

//
// Define the endpoint address of the device to host data IN endpoint.
//

#define CDC_TX_EPADDR (ENDPOINT_DIR_IN | 3)

//
// Define the endpoint address of the host to device data OUT endpoint.
//

#define CDC_RX_EPADDR (ENDPOINT_DIR_OUT | 4)

//
// Define the size in bytes of the CDC device to host interrupt endpoint.
//

#define CDC_NOTIFICATION_EPSIZE 8

//
// Define the size in bytes of the CDC data endpoints.
//

#define CDC_TXRX_EPSIZE 16

//
// ------------------------------------------------------ Data Type Definitions
//

typedef struct {
    USB_Descriptor_Configuration_Header_t Config;
    USB_Descriptor_Interface_t CDC_CCI_Interface;
    USB_CDC_Descriptor_FunctionalHeader_t CDC_Functional_Header;
    USB_CDC_Descriptor_FunctionalACM_t CDC_Functional_ACM;
    USB_CDC_Descriptor_FunctionalUnion_t CDC_Functional_Union;
    USB_Descriptor_Endpoint_t CDC_NotificationEndpoint;
    USB_Descriptor_Interface_t CDC_DCI_Interface;
    USB_Descriptor_Endpoint_t CDC_DataOutEndpoint;
    USB_Descriptor_Endpoint_t CDC_DataInEndpoint;
} USB_Descriptor_Configuration_t;

enum InterfaceDescriptors_t {
    INTERFACE_ID_CDC_CCI,
    INTERFACE_ID_CDC_DCI,
};

enum StringDescriptors_t {
    STRING_ID_Language,
    STRING_ID_Manufacturer,
    STRING_ID_Product
};

//
// -------------------------------------------------------------------- Globals
//

//
// -------------------------------------------------------- Function Prototypes
//
