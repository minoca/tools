/*++

Copyright (c) 2015 Minoca Corp. All Rights Reserved

Module Name:

    pmeter.c

Abstract:

    This module implements the power meter firmware, designed to run on the
    Adafruit ATMega32U4 breakout board connected to an INA219 High Side DC
    Current Sensor Breakout from Adafruit.

Author:

    Evan Green 3-Nov-2015

Environment:

    AVR Firmware

--*/

//
// ------------------------------------------------------------------- Includes
//

#include "pmeter.h"

//
// ---------------------------------------------------------------- Definitions
//

//
// ------------------------------------------------------ Data Type Definitions
//

//
// ----------------------------------------------- Internal Function Prototypes
//

void
SetupHardware (
    void
    );

void
ProcessSerialData (
    void
    );

//
// -------------------------------------------------------------------- Globals
//

//
// Define the instance information for the CDC interface. This is passed to all
// the CDC class driver functions to identify the interface.
//

USB_ClassInfo_CDC_Device_t CdcInterface = {
    .Config = {
        .ControlInterfaceNumber = INTERFACE_ID_CDC_CCI,
        .DataINEndpoint = {
            .Address = CDC_TX_EPADDR,
            .Size = CDC_TXRX_EPSIZE,
            .Banks = 1,
        },

        .DataOUTEndpoint = {
            .Address = CDC_RX_EPADDR,
            .Size = CDC_TXRX_EPSIZE,
            .Banks = 1,
        },

        .NotificationEndpoint = {
            .Address = CDC_NOTIFICATION_EPADDR,
            .Size = CDC_NOTIFICATION_EPSIZE,
            .Banks = 1,
        },
    },
};

//
// Define the file connected to the CDC serial port.
//

FILE UsbSerialStream;

//
// Define the INA219 device instance.
//

INA219 Ina219;

//
// Remember the timer overflow.
//

uint32_t TimerOverflow;
uint16_t TimerPrevious;

//
// ------------------------------------------------------------------ Functions
//

int
main (
    void
    )

/*++

Routine Description:

    This routine implements the entrypoint to the power meter firmware.

Arguments:

    None.

Return Value:

    This routine does not return. If it did, it would probably return 0.

--*/

{

    TimerOverflow = 0;
    SetupHardware();

    //
    // Connect the stream to the CDC interface.
    //

    CDC_Device_CreateStream(&CdcInterface, &UsbSerialStream);
    LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
    GlobalInterruptEnable();
    while (1) {
        ProcessSerialData();
        CDC_Device_USBTask(&CdcInterface);
    }
}

void
EVENT_USB_Device_Connect (
    void
    )

/*++

Routine Description:

    This routine is called when the USB device is connected.

Arguments:

    None.

Return Value:

    None.

--*/

{

    LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
    return;
}

void
EVENT_USB_Device_Disconnect (
    void
    )

/*++

Routine Description:

    This routine is called when the USB device is disconnected.

Arguments:

    None.

Return Value:

    None.

--*/

{

    LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
    return;
}

void
EVENT_USB_Device_ConfigurationChanged (
    void
    )

/*++

Routine Description:

    This routine is called when the USB device's configuration is changed by
    the host.

Arguments:

    None.

Return Value:

    None.

--*/

{

    if (CDC_Device_ConfigureEndpoints(&CdcInterface) != 0) {
        LEDs_SetAllLEDs(LEDMASK_USB_READY);

    } else {
        LEDs_SetAllLEDs(LEDMASK_USB_ERROR);
    }

    return;
}

void
EVENT_USB_Device_ControlRequest (
    void
    )

/*++

Routine Description:

    This routine is called when a control transfer comes in from the host.

Arguments:

    None.

Return Value:

    None.

--*/

{

    CDC_Device_ProcessControlRequest(&CdcInterface);
    return;
}

//
// --------------------------------------------------------- Internal Functions
//

void
SetupHardware (
    void
    )

/*++

Routine Description:

    This routine performs initial hardware configuration.

Arguments:

    None.

Return Value:

    None.

--*/

{

    //
    // Disable the watchdog.
    //

    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    //
    // Disable clock division.
    //

    clock_prescale_set(clock_div_1);

    //
    // Setup timer 1 to run freely.
    //

    TCCR1B |= (1 << CS11);
    TCNT1 = 0;
    LEDs_Init();
    USB_Init();
    I2cInitialize();
    Ina219Initialize(INA219_ADDRESS, &Ina219);
    Ina219Calibrate32V1A(&Ina219);
    return;
}

void
ProcessSerialData (
    void
    )

/*++

Routine Description:

    This routine processes data to and from the USB serial port.

Arguments:

    None.

Return Value:

    None.

--*/

{

    int16_t Character;
    int32_t Current;
    uint16_t Count;
    uint16_t Index;
    uint16_t Time;
    uint32_t TotalTime;

    Count = CDC_Device_BytesReceived(&CdcInterface);
    for (Index = 0; Index < Count; Index += 1) {
        Character = CDC_Device_ReceiveByte(&CdcInterface);
        CDC_Device_SendByte(&CdcInterface, Character + 1);
        CDC_Device_Flush(&CdcInterface);
    }

    Time = TCNT1;
    if (Time < TimerPrevious) {
        TimerOverflow += 0x10000UL;
    }

    TimerPrevious = Time;
    TotalTime = Time | TimerOverflow;
    Current = Ina219GetCurrent(&Ina219);
    fprintf(&UsbSerialStream,
            "%" PRIx32 ",%" PRIi32 "\r\n",
            TotalTime,
            Current);

    CDC_Device_Flush(&CdcInterface);
    return;
}

