/*++

Copyright (c) 2015 Minoca Corp. All Rights Reserved

Module Name:

    i2c.c

Abstract:

    This module implements I2C functionality.

Author:

    Evan Green 3-Nov-2015

Environment:

    AVR firmware

--*/

//
// ------------------------------------------------------------------- Includes
//

#include <avr/io.h>
#include "pmeter.h"

//
// ---------------------------------------------------------------- Definitions
//

#define I2C_CLOCK 400000
#define I2C_PRESCALE ((F_CPU / (2 * I2C_CLOCK)) - 8)

//
// ------------------------------------------------------ Data Type Definitions
//

//
// ----------------------------------------------- Internal Function Prototypes
//

//
// -------------------------------------------------------------------- Globals
//

//
// ------------------------------------------------------------------ Functions
//

void
I2cInitialize (
    void
    )

/*++

Routine Description:

    This routine initializes the two-wire I2C interface.

Arguments:

    None.

Return Value:

    None.

--*/

{

    //
    // Enable pull-ups for SCL/SDA pins. Look out, this might be different for
    // different MCUs.
    //

    PORTD |= (1 << 0) | (1 << 1);
    TWSR = 0;
    TWBR = I2C_PRESCALE;
    TWCR = 1 << TWEN;
    return;
}

void
I2cStart (
    void
    )

/*++

Routine Description:

    This routine sends a start condition out on the I2C bus.

Arguments:

    None.

Return Value:

    None.

--*/

{

    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    while ((TWCR & (1 << TWINT)) == 0) {
        NOTHING;
    }

    return;
}

void
I2cStop (
    void
    )

/*++

Routine Description:

    This routine sends a stop condition out on the I2C bus.

Arguments:

    None.

Return Value:

    None.

--*/

{

    TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
    return;
}

void
I2cWrite (
    uint8_t Value
    )

/*++

Routine Description:

    This routine writes a byte out to the I2C bus.

Arguments:

    Value - Supplies the value to write.

Return Value:

    None.

--*/

{

    TWDR = Value;
    TWCR = (1 << TWINT) | (1 << TWEN);
    while ((TWCR & (1 << TWINT)) == 0) {
        NOTHING;
    }

    return;
}

void
I2cWriteMultiple (
    uint8_t *Buffer,
    int Count
    )

/*++

Routine Description:

    This routine writes multiple bytes out to the I2C bus.

Arguments:

    Buffer - Supplies a pointer to the buffer to write.

    Count - Supplies the number of bytes to write.

Return Value:

    None.

--*/

{

    while (Count != 0) {
        I2cWrite(*Buffer);
        Count -= 1;
        Buffer += 1;
    }

    return;
}

uint8_t
I2cReadNack (
    void
    )

/*++

Routine Description:

    This routine reads a byte in from the I2C bus. It does not acknowledge the
    byte.

Arguments:

    None.

Return Value:

    Returns the read byte.

--*/

{

    TWCR = (1 << TWINT) | (1 << TWEN);
    while ((TWCR & (1 << TWINT)) == 0) {
        NOTHING;
    }

    return TWDR;
}

uint8_t
I2cReadAck (
    void
    )

/*++

Routine Description:

    This routine reads a byte in from the I2C bus, and sends and acknowledge.

Arguments:

    None.

Return Value:

    Returns the read byte.

--*/

{

    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
    while ((TWCR & (1 << TWINT)) == 0) {
        NOTHING;
    }

    return TWDR;
}

void
I2cReadMultiple (
    uint8_t *Buffer,
    int Count
    )

/*++

Routine Description:

    This routine reads multiple bytes out from the I2C bus, ACKing every byte
    except the last one.

Arguments:

    Buffer - Supplies a pointer to the buffer where the bytes will be returned.

    Count - Supplies the number of bytes to read.

Return Value:

    None.

--*/

{

    while (Count > 1) {
        *Buffer = I2cReadAck();
        Count -= 1;
        Buffer += 1;
    }

    if (Count == 1) {
        *Buffer = I2cReadNack();
    }

    return;
}

uint8_t
I2cGetStatus (
    void
    )

/*++

Routine Description:

    This routine gets the status bits from the I2C module.

Arguments:

    None.

Return Value:

    Returns the status bits.

--*/

{

    //
    // Mask out the prescaler bits.
    //

    return TWSR & 0xF8;
}

//
// --------------------------------------------------------- Internal Functions
//

