/*++

Copyright (c) 2015 Minoca Corp. All Rights Reserved

Module Name:

    pmeter.h

Abstract:

    This header contains definitions for the power meter AVR firmware.

Author:

    Evan Green 3-Nov-2015

--*/

//
// ------------------------------------------------------------------- Includes
//

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>

#include "descriptors.h"
#include "ina219.h"

#include <LUFA/Drivers/Board/LEDs.h>
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Platform/Platform.h>

//
// ---------------------------------------------------------------- Definitions
//

#define LEDMASK_USB_NOTREADY LEDS_LED1
#define LEDMASK_USB_ENUMERATING (LEDS_LED2 | LEDS_LED3)
#define LEDMASK_USB_READY (LEDS_LED2 | LEDS_LED4)
#define LEDMASK_USB_ERROR (LEDS_LED1 | LEDS_LED3)

#define NOTHING

//
// ------------------------------------------------------ Data Type Definitions
//

//
// -------------------------------------------------------------------- Globals
//

//
// -------------------------------------------------------- Function Prototypes
//

//
// I2C support functions
//

void
I2cInitialize (
    void
    );

/*++

Routine Description:

    This routine initializes the two-wire I2C interface.

Arguments:

    None.

Return Value:

    None.

--*/

void
I2cStart (
    void
    );

/*++

Routine Description:

    This routine sends a start condition out on the I2C bus.

Arguments:

    None.

Return Value:

    None.

--*/

void
I2cStop (
    void
    );

/*++

Routine Description:

    This routine sends a stop condition out on the I2C bus.

Arguments:

    None.

Return Value:

    None.

--*/

void
I2cWrite (
    uint8_t Value
    );

/*++

Routine Description:

    This routine writes a byte out to the I2C bus.

Arguments:

    Value - Supplies the value to write.

Return Value:

    None.

--*/

void
I2cWriteMultiple (
    uint8_t *Buffer,
    int Count
    );

/*++

Routine Description:

    This routine writes multiple bytes out to the I2C bus.

Arguments:

    Buffer - Supplies a pointer to the buffer to write.

    Count - Supplies the number of bytes to write.

Return Value:

    None.

--*/

uint8_t
I2cReadNack (
    void
    );

/*++

Routine Description:

    This routine reads a byte in from the I2C bus. It does not acknowledge the
    byte.

Arguments:

    None.

Return Value:

    Returns the read byte.

--*/

uint8_t
I2cReadAck (
    void
    );

/*++

Routine Description:

    This routine reads a byte in from the I2C bus, and sends and acknowledge.

Arguments:

    None.

Return Value:

    Returns the read byte.

--*/

void
I2cReadMultiple (
    uint8_t *Buffer,
    int Count
    );

/*++

Routine Description:

    This routine reads multiple bytes out from the I2C bus, ACKing every byte
    except the last one.

Arguments:

    Buffer - Supplies a pointer to the buffer where the bytes will be returned.

    Count - Supplies the number of bytes to read.

Return Value:

    None.

--*/

uint8_t
I2cGetStatus (
    void
    );

/*++

Routine Description:

    This routine gets the status bits from the I2C module.

Arguments:

    None.

Return Value:

    Returns the status bits.

--*/
