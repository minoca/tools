from collections import deque
import csv
import numpy as np
import os
import pylab as plt
import sys
import threading
from time import sleep
import time
from _tkinter import TclError

def main():
    file_count = 0
    data_list = []
    if len(sys.argv) <= 1:
        print("Usage: composite_graph.py [-t graph_title] trace0.csv trace1.csv ...")
        return 1
        
    args = sys.argv[1:]
    graph_title = None
    if args[0] == '-t':
        graph_title = args[1]
        args = args[2:]
        
    for arg in args:
        print("Reading %s" % arg)
        f = open(arg, 'r')
        title = f.readline().strip()
        if title[-1] == ',':
            title = title[:-1]
            
        f.readline()
        incsv = csv.reader(f)
        data = deque([])
        average = 0.0
        count = 0
        for line in incsv:
            if len(line) == 2:
                try:
                    float(line[0])
                    average += float(line[1])
                    data.append(line)
                    count += 1
                    
                except ValueError:
                    pass
            
        data_list.append({'data': data, 'title': title})
        average = average / count
        print("%s: Average current %f across %d samples" % (title, average, count))
        f.close()
        file_count += 1
        
    print("Read %d files" % file_count)
    graph = plt.figure()
    if graph_title:
        plt.title(graph_title)
        
    plt.xlabel("Time (seconds)")
    plt.ylabel("Current (mA)")
    ax1 = graph.add_subplot(111)
    for data_dict in data_list:
        data = data_dict['data']
        x, y = zip(*data)
        ax1.plot(x, y, label=data_dict['title'])
        
    if file_count > 1:
        plt.legend(loc='lower left')
        
    print("Showing graph")
    plt.show()
    print("Exiting")
    return 0
    
if __name__ == "__main__":
    exit(main())
    