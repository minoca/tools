from collections import deque
import csv
import numpy as np
import os
import pylab as plt
import sys
import threading
from time import sleep
import time
from _tkinter import TclError

PLOT_POINTS = 200000

class ReaderThread(threading.Thread):
    def __init__(self, input):
        super(ReaderThread, self).__init__()
        self.stop_event = threading.Event()
        self.input = input
        self.data = deque([], PLOT_POINTS)
        
    def stop(self):
        self.stop_event.set()
        
    def stopped(self):
        return self.stop_event.isSet()
        
    def run(self):
        current_time = time.strftime("%m/%d/%Y %H:%M:%S")
        print("Beginning graph at %s" % current_time)
        incsv = csv.reader(self.input)
        while not self.stopped():
            try:
                line = incsv.next()
                if len(line) < 2:
                    continue
                   
                tstamp = float(line[0])
                milliamps = float(line[1])
                # Skip really crazy ones.
                if milliamps > 1600:
                    print "skipping %d" % milliamps
                    continue
                    
                self.data.append(line)
                
            except ValueError:
                break

        print("IO thread exiting")
        return
    
def main():
    print sys.argv
    if len(sys.argv) == 2:
        path = sys.argv[1]
        if path[:3].lower() == 'com':
            path = '\\\\.\\' + path
        
        print("Opening input %s" % path)
        input = open(path, 'rb')    
        
    else:
        print("Using stdin as input")
        input = sys.stdin
                
    plt.ion()
    reader = ReaderThread(input)
    reader.start()
    graph = plt.plot([], [])[0]
    plt.title("Power")
    plt.xlabel("Time (s)")
    plt.ylabel("Current (mA)")
    ax = plt.gca() 
    try:
        while len(reader.data) == 0:
            plt.pause(0.01)
    
        print("Got data!")
        while True:
            x, y = zip(*reader.data)
            graph.set_ydata(y)
            graph.set_xdata(x)
            ax.relim()
            ax.autoscale_view()
            plt.draw()
            plt.pause(0.01)
            
    except (KeyboardInterrupt, TclError):
        print "Stoppit!"
        
    print("Cleaning up")
    reader.stop()
    reader.join()
    if input != sys.stdin:
        input.close()
        
    print("Exiting")
    return 0
    
if __name__ == "__main__":
    exit(main())